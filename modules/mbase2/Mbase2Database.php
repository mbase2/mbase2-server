<?php
final class Mbase2Database {
  private static $conn = null;
  public static $uid = null;
  private static $mb2sys = 'mbase2';
  private static $mb2data = 'mb2data';

  private static $exceptionMessages = [
    'empty_vid' => 'Virtual table does not exist.'
  ];

  public static function convertNamedParametersToPositional($sql, $params) {
    $parNames = array_keys($params);

    $positions = [];

    foreach ($parNames as $i => $par) {
      $pos = '\$'.($i+1);
      $positions[] = $pos;

      $sql = preg_replace('/'.$par.'\b/', $pos, $sql); //The \b matches a word boundary. Parentheses are not counted as part of a word.
      /**
       * e.g.: 
       * command:
       *  echo preg_replace('/:a1\b/','X',':a1) :a10');
       * output: 
       *  X) :a10
       */
    }

    return [$sql, array_values($params)];
  }

  /**
   * Helper function to get the field from resultset array
   * @param {array} $res resultset as returned from Mbase2Database::query
   * @field {string} $field field name to retrieve
   * $inx {integer} row index to retrieve
   */

  public static function fetchField ($res, $field, $inx = 0) {
    if (isset($res[0])) {

      $r = $res[0];

      if (array_key_exists($field, $r)) {
        return $r[$field];
      }
      
    }

    return false;
  }

  public static function getIntersection($tname, $point) {
    return self::query("SELECT * FROM mbase2_ge.$tname WHERE ST_Intersects(geom,ST_SetSRID(ST_MakePoint(:lon, :lat),4326))", $point);
  }

  /**
   * Connects directly to the database - currently not used as we are connecting to the Drupal db connection
   */
  public static function connect($host = 'localhost', $port = '5432', $dbname = 'portal_mbase_org', $mb2sys = 'mbase2',  $username = 'mbase_org', $password = 'some_password') {
    self::$conn = pg_connect("host={$host} port={$port} dbname={$dbname} user={$username} password={$password}");
  }

  public static function query($sql, $params = [], $result_type = PGSQL_ASSOC) {

    if (!empty(self::$conn) || (!function_exists('db_query') && !empty($params))) {
      list($sql, $params) = self::convertNamedParametersToPositional($sql, $params);
    }

    if (function_exists('db_query') && empty(self::$conn)) {
      
      $result = db_query($sql, $params);

      if ($result_type === PGSQL_ASSOC) {
        $result_type = PDO::FETCH_ASSOC;
      }
      else if ($result_type === PGSQL_NUM) {
        $result_type = PDO::FETCH_NUM;
      }
      else if ($result_type === PGSQL_BOTH) {
        $result_type = PDO::FETCH_BOTH;
      }

      return $result->fetchAll($result_type);

    }
    else {
      $result = pg_query_params($sql, $params);

      if ($result === false) {
        throw new Exception(pg_last_error());
      }
      
      //$result_type was added to pg_fetch_all with PHP version 7.1
      //return pg_fetch_all ($result, $result_type);
      $rval = [];
      while ($row = pg_fetch_array($result, NULL, $result_type)) {
          $rval[] = $row;
      }
      return $rval;
    }
  }

  public static function tableExists($schema, $tname) {

    $res = self::query("SELECT EXISTS (
      SELECT FROM pg_tables
      WHERE  schemaname = :schema
      AND    tablename  = :tname
      );", [':schema'=>$schema, ':tname'=>$tname]);
      
      return $res[0]['exists'];
  }

  public static function updateSchema($moduleId, $schema = 'mb2data', $defaultVariables = true) {
    
    if (empty($moduleId)) {
      return [];
    }

    $mb2sys = 'mbase2';
    
    $p = [':moduleId' => $moduleId];
    $res = self::query("SELECT key FROM $mb2sys.code_list_options WHERE id=:moduleId",$p);
    $tname = $res[0]['key'];
    if (empty($tname)) throw new Exception("Missing table name.");

    $selectDefault = '';

    if ($defaultVariables===true) {
      $selectDefault = "or module_id = (SELECT id from mbase2.code_list_options WHERE key='__default_module')";
    }

    $res = self::query("SELECT * FROM $mb2sys.module_variables WHERE module_id=:moduleId $selectDefault" , $p);

    if (empty($res)) return $res;

    if (empty(self::query("SELECT id from mbase2.modules where id=:moduleId", $p))) {  //remove default variables from the result
      foreach ($res as $inx => $cdef) {
        if (is_null($cdef['module_id'])) {
          unset($res[$inx]);
        }
      }
    }

    if (empty($res)) return $res;

    self::translateRowsColumns($res, ['data_type_id', 'name_id'], null);  //this is needed to get keys from ids

    self::query("CREATE TABLE IF NOT EXISTS $schema.$tname (id serial PRIMARY KEY)");
    
    /*
    $constraints = self::getConstraints($schema, $tname);
    
    foreach ($constraints as $constraint) {
      self::query("ALTER TABLE $schema.$tname DROP CONSTRAINT $constraint");
    }
    */

    $locationReferenceColumn = null;

    $tableReferences = null;

    foreach ($res as $cdef) {
      
      if ($cdef['form_data'] === true) {
        continue;
      }

      $unique = $cdef['unique_constraint'] ? "UNIQUE" : "";
      //$required = $cdef['required'] ? "NOT NULL" : "";
      $required = '';
      $type = $cdef['key_data_type_id'];
      $default = $cdef['default_value'] ? " DEFAULT {$cdef['default_value']}" : '';
      $references = "";

      $cname = $cdef['key_name_id'];

      if ($type === 'code_list_reference') { 
        $type = 'integer';
        $references = "references $mb2sys.code_list_options(id)";
      }
      else if ($type === 'table_reference') {
        $type = 'integer';

        if (is_null($tableReferences)) {
          $tableReferences = self::query("SELECT * from mbase2.view_referenced_tables");
          $tableReferences = array_column($tableReferences, null, 'id');
        }

        if (isset($tableReferences[$cdef['ref']])) {
          $ref = $tableReferences[$cdef['ref']];
          
          if (self::tableExists($ref['schema'], $ref['key'])) {
            $references = "references {$ref['schema']}.{$ref['key']}({$ref['pkey']})";
          }
        }
      }
      else if ($type === 'location_reference') {
        $locationReferenceColumn = $cname;
        $type = 'integer';
        //$references = "references mb2data._locations(id)";  //geom tables are now splitted between modules
      }
      else if ($type === 'image_array') {
        $type = 'jsonb';
        //create index on mb2data.ct using gin ((photos)); //https://stackoverflow.com/questions/19925641/check-if-a-postgres-json-array-contains-a-string
      }
      else if ($type === 'image') {
        $type = 'text';
      }
      else if ($type ==='table_reference_array') {
        $type = 'jsonb';
      }
      else if ($type === 'code_list_reference_array') {
        $type = 'jsonb';
      }
      else if ($type === 'email') {
        $type = 'text';
      }
      else if ($type === 'phone') {
        $type = 'text';
      }
      else if ($type === 'json') {
        $type = 'jsonb';
      }
      else if ($type === 'location_geometry') {
        $type = 'geometry';
      }

      if (!self::columnExists($schema, $tname, $cname)) {
        self::quiet_query("ALTER TABLE $schema.$tname ADD COLUMN $cname $type $unique $required $default");
        !empty($references) && self::quiet_query("ALTER TABLE $schema.$tname ADD FOREIGN KEY ($cname) $references");
        continue;
      }

      if (empty($default)) {
        self::quiet_query("ALTER TABLE $schema.$tname ALTER COLUMN $cname DROP DEFAULT;");
      }
      else {
        self::quiet_query("ALTER TABLE $schema.$tname ALTER COLUMN $cname SET $default;");
      }

      self::quiet_query("ALTER TABLE $schema.$tname ALTER COLUMN $cname TYPE $type USING $cname::$type");    
      
      //change uniqueness
      if (empty($unique)) {
        self::query("ALTER TABLE $schema.$tname DROP CONSTRAINT IF EXISTS {$tname}_{$cname}_unique_constraint");
      }
      else {
        self::quiet_query("ALTER TABLE $schema.$tname ADD CONSTRAINT {$tname}_{$cname}_unique_constraint UNIQUE ($cname)");
      }
      
      //nullable
      if (empty($required)) {
        self::query("ALTER TABLE $schema.$tname ALTER COLUMN $cname DROP NOT NULL;");
      }
      else {
        self::quiet_query("ALTER TABLE $schema.$tname ALTER COLUMN $cname SET NOT NULL");
      }
        
      //references
      $fkey = self::getForeignKey($schema, $tname, $cname);
      if (!empty($fkey)) {
        self::query("ALTER TABLE $schema.$tname DROP CONSTRAINT $fkey");
      }

      if (!empty($references)) {
        self::quiet_query("ALTER TABLE $schema.$tname ADD FOREIGN KEY ($cname) $references");
      }
      
    }

    self::quiet_query("set schema 'mbase2';
      SET search_path TO public, mbase2;

      DROP MATERIALIZED VIEW IF EXISTS mbase2.code_list_options_fkeys;
      CREATE MATERIALIZED VIEW mbase2.code_list_options_fkeys AS
      SELECT * FROM (
      SELECT conrelid::regclass AS FK_Table
            ,CASE WHEN pg_get_constraintdef(c.oid) LIKE 'FOREIGN KEY %' THEN substring(pg_get_constraintdef(c.oid), 14, position(')' in pg_get_constraintdef(c.oid))-14) END AS FK_Column
            ,CASE WHEN pg_get_constraintdef(c.oid) LIKE 'FOREIGN KEY %' THEN substring(pg_get_constraintdef(c.oid), position(' REFERENCES ' in pg_get_constraintdef(c.oid))+12, position('(' in substring(pg_get_constraintdef(c.oid), 14))-position(' REFERENCES ' in pg_get_constraintdef(c.oid))+1) END AS PK_Table
            ,CASE WHEN pg_get_constraintdef(c.oid) LIKE 'FOREIGN KEY %' THEN substring(pg_get_constraintdef(c.oid), position('(' in substring(pg_get_constraintdef(c.oid), 14))+14, position(')' in substring(pg_get_constraintdef(c.oid), position('(' in substring(pg_get_constraintdef(c.oid), 14))+14))-1) END AS PK_Column
      FROM   pg_constraint c
      JOIN   pg_namespace n ON n.oid = c.connamespace
      WHERE  contype = 'f' 
      AND (nspname = 'mbase2' OR nspname = 'mb2data')
      AND pg_get_constraintdef(c.oid) LIKE 'FOREIGN KEY %'
      ORDER  BY pg_get_constraintdef(c.oid), conrelid::regclass::text, contype DESC
      )a where PK_Table = 'code_list_options';");

    return $res;
  }

  private static function quiet_query($sql, $params=[]) {
    try{
      self::query($sql, $params);
    }
    catch (Exception $e) {
    }
  }

  public static function getForeignKey($schema, $tname, $cname) {
    //https://stackoverflow.com/questions/1152260/postgres-sql-to-list-table-foreign-keys
    $res = self::query("SELECT conname,
      pg_catalog.pg_get_constraintdef(r.oid, true) as condef
      FROM pg_catalog.pg_constraint r
      WHERE r.conrelid = '$schema.$tname'::regclass AND 
      conname like '%_{$cname}_%' AND
      r.contype = 'f' ORDER BY 1");

    return $res[0]['conname'];
  }

  public static function columnExists($schema, $tname, $cname) {
    $res = self::query("SELECT column_name 
      FROM information_schema.columns 
      WHERE table_schema = :schema AND
      table_name=:tname AND 
      column_name=:cname;",
      [
        ':schema'=>$schema,
        ':tname'=>$tname,
        ':cname'=>$cname
      
      ]);
    
    return !empty($res);
  }

  /**
   * function paramsToConditions(&$params)
   * Returns WHERE statement and turns $params input argument to appropriate placeholder array of values
   * Example:
   * $params = [':list_id' => [6,1,2,3,4]];
   * 
   * Returns: list_id IN (:__mbase2_list_id_0,:__mbase2_list_id_1,:__mbase2_list_id_2,:__mbase2_list_id_3,:__mbase2_list_id_4)
   * 
   * and changes $params to:
   * Array (
    * [:__mbase2_list_id_0] => 6
    * [:__mbase2_list_id_1] => 1
    * [:__mbase2_list_id_2] => 2
    * [:__mbase2_list_id_3] => 3
    * [:__mbase2_list_id_4] => 4
    * )
   */
  public static function paramsToConditions(&$params) {
    
    $conditions = [];

    foreach ($params as $key => $value) {
      $pos = strpos($key, ':', 1);  //if $pos !== false we have a foreign key condition

      if ($value[0] === '(') {
        $value = explode(',', trim($value,"()"));
      }

      $conditionOperator = '=';
      $conditionPlaceholder = $conditionPlaceholderKey = $pos === false ? $key : substr($key, 0, $pos);
      $placeholderValues = [];

      $key0 = trim($conditionPlaceholder, ':');

      //api accepts multiple values in parantheses
      
      if (is_array($value)) {
        foreach ($value as $i => $v) {
          $placeholderValues[':__mbase2_'.str_replace('.','_',$key0).'_'.$i] = $v;
        }
        $conditionOperator = ' IN ';
        $conditionPlaceholder = '('.implode(',', array_keys($placeholderValues)).')';
      }

      if ($pos === false) {
        $conditions[] = $key0.$conditionOperator.$conditionPlaceholder; //label_id=:label_id : label_id IN (:__mbase2_1, :__mbase2_2, ...)
      }
      else { //foreign key value
        $p = explode('/', substr($key, $pos+1));
        $subq = "(SELECT id FROM mbase2.{$p[0]} WHERE {$p[1]} $conditionOperator $conditionPlaceholder)";
        $conditions[] = $key0.$conditionOperator.$subq;
        $params[$conditionPlaceholderKey] = $value;  //this assignment will be unset if $value is array (because then $placeholderValues will are set)
        unset($params[$key]);
      }

      if (count($placeholderValues) > 0) {
        foreach ($placeholderValues as $placeholder => $placeholderValue) {
          $params[$placeholder] = $placeholderValue;
        }
        unset($params[$conditionPlaceholderKey]);
      }
    }

    return implode('AND ', $conditions);
  }

  public static function paramsToUpdateSet(&$params) {
    $cols = [];

    foreach (array_keys($params) as $key) {
      
      if ($key === ':id') continue;

      $value = &$params[$key];

      if ($key === ':_location') {
        $cols[] = '_location = ST_SetSRID(ST_MakePoint(:lon,:lat),4326)';
        $params[':lat'] = $value['lat'];
        $params[':lon'] = $value['lon'];
        unset($params[':_location']);
        self::updateLocationData($params, $value);
        $cols[] = '_location_data = :_location_data';
      }
      else {
        $cols[] = trim($key,':').'='.$key;
        if (is_array(($value))) $value = json_encode($value);
      }
    }
    unset($value);
    return implode(',', $cols);
  }

  private static function getLocationsTable(&$location) {
    $locationsTable =  '_locations';
      
    if (isset($location['_locations_table'])) {
      $locationsTable = $location['_locations_table'];
      unset($location['_locations_table']);
    }

    return $locationsTable;
  }

  private static function updateLocationData(&$params, $value) {
    $data = isset($params[':_location_data']) ? $params[':_location_data'] : [];
    $data['_location'] = $value;
    $params[':_location_data'] = json_encode($data);
  }

  private static function updateLocationReferenceGeometry(&$params) {
    $res = null;
    $rlocation = null;
    if (isset($params[':_location_reference'])) {
      $location = $params[':_location_reference'];
      $locationsTable = self::getLocationsTable($location);
      if (isset($location['id']) && strlen($location['id'])) {
        $res = self::query("UPDATE mb2data.$locationsTable 
          SET geom = ST_SetSRID(ST_MakePoint(:lon,:lat),4326)
          WHERE id = :id RETURNING id, st_asgeojson(geom) geom",[
            ':id'=>$location['id'],
            ':lat'=>$location['lat'],
            ':lon'=>$location['lon']
          ]);
      }
      else {
        $res = self::query("INSERT INTO mb2data.$locationsTable (location_accuracy, geom) 
            VALUES (:location_accuracy, ST_SetSRID(ST_MakePoint(:lon,:lat),4326)) RETURNING id, st_asgeojson(geom) geom", 
            [
              ':location_accuracy' => $location['location_accuracy'],
              ':lat' => $location['lat'],
              ':lon' => $location['lon']
          ]);
      }
      $rlocation = $res[0]['geom'];
    }

    if (!is_null($res)) {
      $id = $res[0]['id'];
      $params[':_location_reference'] = $id;
      return ['id' => $id,'geom'=>$rlocation];
    }

    return null;
  }

  public static function delete($tname, $params = [], $language = null, $result_type = PGSQL_ASSOC) {

    if (empty($params)) return [];

    $id = $params[':id'];

    if (empty($id)) return [];
    
    $res = self::query("DELETE FROM {$tname} WHERE id=:id RETURNING *", $params, $result_type);

    return $res;
  }

  public static function update($tname, $params = [], $language = null, $result_type = PGSQL_ASSOC) {

    if (empty($params)) return [];

    $id = $params[':id'];

    if (empty($id)) return [];

    foreach ($params as $key => &$value) {
      if (strlen($value)===0) {
        $params[$key] = null;
      }
      else if (self::recordUserName($key, $tname)) {
        $value = self::$uid;
      }
    }

    unset($value);

    $g = self::updateLocationReferenceGeometry($params);

    $cols = self::paramsToUpdateSet($params);

    if (empty($cols)) throw new Exception("Columns for update are not defined.");
    
    $res = self::query("UPDATE {$tname} SET $cols WHERE id=:id RETURNING *", $params, $result_type);

    if (!is_null($g)) {
      $res[0]['geom'] = $g['geom'];
    }

    return $res;
  }

  public static function insert($tname, $params = [], $language = null, $result_type = PGSQL_ASSOC) {
    
    if (empty($params)) return [];

    $multipleInsert = [];

    foreach ($params as $key => $value) {
      if (is_numeric($key) && is_array($value)) {
        try {
          $multipleInsert[] = self::insert($tname, $value, $language, $result_type);
        }
        catch (Exception $e) {
          ;
        }
      }
      else {
        if (strlen($value)===0) {
          $params[$key] = null;
        }
      }
    }

    if (!empty($multipleInsert)) return $multipleInsert;
    
    $valueKeys = [];
    $cnames = [];
    $location = null;

    foreach (array_keys($params) as $key) {

      $value = &$params[$key];
      
      if ($key === ':_location_reference') {
        
        $locationsTable = self::getLocationsTable($value);

        if (strlen($value['id'])>0) {
          $value = $value['id'];
        }
        else {
          $lat = $value['lat'];
          $lon = $value['lon'];
          if (empty($lat) || empty($lon)) {
            unset($params[$key]);
            continue;
          }
          $res = self::query("INSERT INTO mb2data.$locationsTable (location_accuracy, geom) 
            VALUES (:location_accuracy, ST_SetSRID(ST_MakePoint(:lon,:lat),4326)) RETURNING id, st_asgeojson(geom) geom", 
            [
              ':location_accuracy' => $value['location_accuracy'],
              ':lat' => $lat,
              ':lon' => $lon
          ]);
          $location = $res[0]['geom'];
          $value = $res[0]['id'];
        }
      }
      else if (self::recordUserName($key, $tname)) {
        $value = self::$uid;
      }
      
      $cnames[] = trim($key, ':');
      
      if ($key === ':_location') {
        $valueKeys[] = 'ST_SetSRID(ST_MakePoint(:lon,:lat),4326)';
        $params[':lat'] = $value['lat'];
        $params[':lon'] = $value['lon'];
        unset($params[$key]);
        self::updateLocationData($params, $value);
        
        $cnames[] = '_location_data';
        $valueKeys[]=':_location_data';
      }
      else {
        $valueKeys[] = $key;
        if (is_array(($value))) $value = json_encode($value);
      }
    }

    unset($value);

    $cnames = implode(',', $cnames);
    $valueKeys = implode(',', $valueKeys);
    
    $res = self::query("INSERT INTO {$tname} ($cnames) VALUES ($valueKeys) RETURNING *", $params, $result_type);
    
    if (!is_null($location)) {
      $res[0]['geom'] = $location;
    }

    return $res;
  }

  /**
   * Returns true if provided uid or uname value should be ignored and replaced by self::$uid
   */
  private static function recordUserName($key, $tname) {
    return ($key === ':_uid' || $key === ':_uname') && !in_array($tname, ['mb2data.dmg_deputies']);
  }

  /**
   * @param {array} associative array of TRANSLATED(! - attribute "key_id" is expected to be present in each row) rows from mbase2.referenced_tables
   * @param {object} overrides (for camelot) - sql statements to be executed instead of primery definition in referenced tables
   */

  public static function getReferencedValues($tableReferences, $overrides = []) {

    $res = [];
    
    foreach($tableReferences as $tr) {
      $tname = $tr['key_id'];
      $schema = $tr['schema'];
      $cname = $tr['label_key'];
      $key = $tr['pkey'];
      $listId = $tr['id'];
      $additional = $tr['additional'];
      $from = empty($tr['select_from']) ? $schema.'.'.$tname : '('.$tr['select_from'].') a';

      $additional = empty($additional) ? "" : ", $additional";

      try {
        if (isset($overrides[$tname])) {
          $r = self::query($overrides[$tname]['sql'],$overrides[$tname]['params']);
        }
        else {
          $r = self::query("SELECT $key as id, $listId as list_id, '$tname' as list_key, $cname as key $additional FROM $from");
        }
        if (!empty($r)) {
          $res = array_merge($res, $r);
        }
      } catch (Exception $e) {
        return ['err' => $e];
      }
    }

    return $res;
  }

  public static function select($tname, $params = [], $language = null, $result_type = PGSQL_ASSOC) {

    $filter = [];
    
    if (isset($params[':__filter'])) {
      $filter = json_decode($params[':__filter']);
      unset($params[':__filter']);
    }

    $cols = '*';

    if (isset($params[':__select'])) {
      $cols = $params[':__select'];
      unset($params[':__select']);
    }
    
    $where = '';
    if (!empty($params)) {
      $where = ' WHERE '.self::paramsToConditions($params);
    }

    if (!empty($filter)) {
      if (empty($where)) {
        $where = ' WHERE ';
      }
      
      $filterWhere = [];
      foreach($filter as $i => $f) {
        $p = ":__filter$i";
        $filterWhere[] = "{$f[0]}{$f[1]}$p";
        $params[$p] = $f[2]; 
      }

      $where.=implode(' AND ', $filterWhere);
    }

    $res = self::query("SELECT $cols from {$tname} $where", $params, $result_type);

    return self::translateRows($res, $language, $tname);
  }

  public static function translateRows($res, $language, $tname) {
    if (empty($res) || is_null($language)) return $res;

    $cnames = self::query("SELECT fk_column from mbase2.code_list_options_fkeys where fk_table = :tname::regclass", [':tname'=>$tname]); //column names for translation

    if (empty($cnames)) return $res;

    $cnames = array_column($cnames, 'fk_column');

    self::translateRowsColumns($res, $cnames, $language);

    return $res;
  }

  /**
   * Translates $cnames in array of rows objects
   * 
   * @param &$res {array<object>} array of associative object rows
   * @param $cnames {array<string>} column names for translations
   * @param $language {string} language code
   */
  private static function translateRowsColumns(&$res, $cnames, $language) {
    
    $ids = [];
    foreach ($res as $row) {
      foreach ($cnames as $cname) {
        $ids[] = $row[$cname];
      }
    }

    $p = [':id' => $ids];

    $translations = self::query("SELECT id, key, values->>'$language' t FROM mbase2.code_list_options WHERE ".self::paramsToConditions($p),$p);

    $translations = array_column($translations, null, 'id');

    foreach ($res as &$row) {
      foreach ($cnames as $cname) {
        $trans = $translations[$row[$cname]];
        $row['t_'.$cname] = $trans['t'];
        $row['key_'.$cname] = $trans['key'];
      }
    }

    unset($row);
  }

  public static function virtualTableAttributes($tname, $language = null) {
    $vid = intval($tname);

    if ($vid === 0) {
      $vid = self::getVirtualTableId($tname);
    }

    $attributes = self::getVirtualTableAttributes($vid);

    if (is_null($language)) {
      return $attributes;
    }
    
    self::translateRowsColumns($attributes, ['id'], $language);

    return $attributes;
    
  }

  public static function virtualTableValues($tname, $language = null) {
    $vid = self::getVirtualTableId($tname);
    
    $attrs = self::getVirtualTableAttributes($vid);
    $values = self::_virtual_select($vid, $attrs);

    //scan the attributes list to get attributes holding code list values
    $codeLists = [];
    foreach($attrs as $attr) {
        $ref = $attr['ref'];
        if (isset($ref)) {
          $ref = json_decode(($ref));
          if ($ref[0] === 'code_lists') {
            $codeLists[$attr['label_key']] = $ref[1];
          }
        } 
    }
        
    if (count($codeLists) > 0) {
      $codeListsCondition = implode(',', $codeLists);
      $codeListsValues = array_column(self::select('code_list_options',[':list_id'=>"($codeListsCondition)"]), NULL, 'id');
      
      foreach ($values as $attrKey => &$entValues) {
        if (isset($codeLists[$attrKey])) {
          foreach ($entValues as &$value) {
            $clistRow = $codeListsValues[$value['value']];
            $translation = json_decode($clistRow['values'], true);
            $value['key'] = $clistRow['key'];
            if (isset($translation)) {
              $value['value'] = empty($translation[$language]) ? $value['key'] : $translation[$language];
            }
          }
          unset($value);
        }
      }
      unset($entValues);
    }

    return $values;
  }

  public static function getVirtualTableId($tname) {
    list($vid) = self::query("SELECT vt.id as vid FROM mbase2.code_list_options clv,mbase2.virtual_tables vt WHERE key = :tname AND clv.id = vt.id", [':tname'=>$tname]);
    $vid = $vid['vid'];
    if (empty($vid)) throw new Exception(self::$exceptionMessages['empty_vid']);
    return $vid;
  }

  public static function getVirtualTableAttributes($vid) {
    return self::query("SELECT a.* FROM mbase2.attributes a, mbase2.virtual_table_attributes v WHERE v.attribute_id = a.id AND virtual_table_id = :vid",[':vid' => $vid]);
  }

  public static function virtual_select($tname, $params = [], $language = null, $result_type = PGSQL_ASSOC) {
    $vid = self::getVirtualTableId($tname);
    $attributes = self::getVirtualTableAttributes($vid);
    return self::_virtual_select($vid, $attributes, $params, $result_type);
  }

  public static function _virtual_select($vid, $attributes, $params = [], $language = null, $result_type = PGSQL_ASSOC ) {
    $res = [];
    $entIds = []; //here we store the ent_id of items which are filtered to narrow the selection of rows from the EAV model

    //filter
    $filterLabelKeys = []; //here we store the attributes by which the selection was already filtered

    //first we select the values by the filter to narrow the selection of values without the filter
    foreach ($attributes as $attr) {
      $labelKey = $attr['label_key'];
      $where = @$params[':'.$labelKey];
      $parVal = [':vid'=>$vid, ':aid'=>$attr['id']];
      if (isset($where)) {
        $parVal[':value'] = $where;
        $where = "AND value=:value";
        $filterLabelKeys[$labelKey] = true;

        $r = self::query("SELECT ent_id, value FROM mbase2.att_{$attr['data_type_key']} WHERE vtb_id = :vid AND att_id=:aid $where", $parVal, $result_type);
        $entIds = array_merge($entIds, array_column($r,'ent_id'));
        $res[$labelKey] = $r;
      }
    }

    $where = '';

    if (!empty($entIds)) {
      $where = 'AND ent_id IN ('.implode(',', $entIds).')';
    }

    foreach ($attributes as $attr) {
      $labelKey = $attr['label_key'];
      if (isset($filterLabelKeys[$labelKey])) continue; //skip the attributes already processed in the previous loop
      $parVal = [':vid'=>$vid, ':aid'=>$attr['id']];
      $res[$labelKey] = self::query("SELECT ent_id, value FROM mbase2.att_{$attr['data_type_key']} WHERE vtb_id = :vid AND att_id=:aid $where", $parVal);
    }

    return $res;
    //optionally group the results by the EAV ent_id (corresponds to a row in relational model) - TODO if a need arises   
  }

  public static function virtual_update($tname, $params = [], $language = null, $result_type = PGSQL_ASSOC) {
    $vid = self::getVirtualTableId($tname);
    $attributes = self::getVirtualTableAttributes($vid);

    $res = [];
    $id = $params[':id'];

    if (empty($id)) return ['err' => 'Missing :id for a record to update.'];
    
    foreach ($attributes as $attr) {
      $labelKey = $attr['label_key'];
      $where = @$params[':'.$labelKey];
      $parVal = [':vid'=>$vid, ':aid'=>$attr['id'], ':eid' => $id];
      if (isset($where)) {
        $parVal[':value'] = $where;
        $res[$labelKey] = self::query("UPDATE mbase2.att_{$attr['data_type_key']} SET value=:value WHERE vtb_id = :vid AND att_id=:aid AND ent_id=:eid RETURNING *", $parVal);
      }
    }

    return $res;
  }

  // {{{ virtual_insert()
    /**
     * Inserts an entity to EAV database tables
     * @param string $tname virtual table name for the EAV model of the insertion. $tname has to be present in mbase2.virtual_tables table
     * @param Array $params associative array of values to insert. Array keys have to be present in code_list_options with variables code_list before the insertion
     */

    public static function virtual_insert($tname, $params = [], $language = null, $result_type = PGSQL_ASSOC) {
      //$params = json_decode('{"camera_make": "abc1", "camera_model": "abc2", "name": "trap station 1", "location": {"type":"Point","coordinates":[15,45]}}', true);
      //geojson order of coordinates: The first two elements are longitude and latitude, or easting and northing

      //get virtual table ID
      $vid = self::getVirtualTableId($tname);

      //get ID for a new EAV record
      $seq = 'mbase2_'.$tname.'_seq';
      list($entId) = self::query("SELECT nextval('$seq');",[], PGSQL_NUM)[0];
      if (empty($entId)) return ['err' => 'Can not set ID for a new entity'];

      //get data types for attributes
      $attributes = self::getVirtualTableAttributes($vid);
      $attributesAssoc = [];
      foreach ($attributes as $attr) {
        $attributesAssoc[$attr['label_key']] = ['id' => $attr['id'], 'data_type_key' => $attr['data_type_key']];
      }

      $inserts = []; // group inserts by data_type for multiple insert
      $placeholderCount = 0;
      foreach ($params as $key => $p) {
        $key = trim($key, ':');
        $dataType = $attributesAssoc[$key]['data_type_key'];  //dataType for attribute (variable) value
        $attId = $attributesAssoc[$key]['id'];
        if (!isset($inserts[$dataType])) {
          $inserts[$dataType] = ['placeholders' => [], 'values' => []];
        }
        
        $start = $placeholderCount;
        $end = $start + 3;
        $placeholderCount = $placeholderCount + 4;

        $placeholders = array_map(function ($i) { return ':p'.$i;}, range($start, $end));

        $values = [
          $placeholders[0] => $vid,
          $placeholders[1] => $entId,
          $placeholders[2] => $attId
        ];

        if ($dataType === 'geometry') {
          $values[$placeholders[3]] = json_encode($p);
          $placeholders[3] = "ST_GeomFromGeoJSON({$placeholders[3]})";
        }
        else {
          $values[$placeholders[3]] = $p;
        }

        $inserts[$dataType]['values'][] = $values;
        $inserts[$dataType]['placeholders'][] = '('.implode(',', $placeholders).')';
      }

      $rval = [];
      foreach ($inserts as $dataType => $insert) {
        $placeholders = implode(',', $insert['placeholders']);
        $values = call_user_func_array('array_merge', $insert['values']);
        $sql = "INSERT INTO mbase2.att_{$dataType} (vtb_id, ent_id, att_id, value) VALUES $placeholders RETURNING *";
        $rval[]=self::query($sql, $values);
      }
      return $rval;
    }

    public static function getConstraints($schema, $tname, $cond = "contype <> 'p'") {
      if (!empty($cond)) $cond = "AND $cond";
      $res = self::query("SELECT con.conname
       FROM pg_catalog.pg_constraint con
            INNER JOIN pg_catalog.pg_class rel
                       ON rel.oid = con.conrelid
            INNER JOIN pg_catalog.pg_namespace nsp
                       ON nsp.oid = connamespace
       WHERE nsp.nspname = :schema $cond
             AND rel.relname = :tname",[':schema' => $schema, ':tname'=>$tname]);

      return array_column($res, 'conname');
    }

    public static function moduleReferences($moduleId, &$ids) {
        $res = self::query("SELECT v.ref FROM mbase2.module_variables v, mbase2.code_list_options clv, mbase2.referenced_tables rt WHERE 
        module_id = :module_id AND 
        ref is not null AND
        clv.list_key = 'referenced_tables' AND
        rt.id = clv.id AND 
        rt.static = false AND
        ref = clv.id", 
        [':module_id' => $moduleId]);
        
        foreach ($res as $r) {
          $id = $r['ref'];
          $ids[] = $id;
          self::moduleReferences($id, $ids);
        }
    }

  // }}}
  
}
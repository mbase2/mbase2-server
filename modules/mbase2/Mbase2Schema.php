<?php

    class Mbase2Schema {

        static $translations = [];
        static $patterns = [];
        static $codeListValues = [];
        static $exceptions = [];
        
        static function tableDefinition($def) {
            if (!isset($def->type)) $def->type = 'referenced_tables';
            $moduleId = self::getKeyId($def->type, $def->table_name);
            if ($moduleId === false) {
                $listId = self::getCodeListId($def->type);
                $res = Mbase2Database::query("INSERT INTO mbase2.code_list_options (list_id, key, values)
                VALUES (:list_id, :key, :values) RETURNING id", 
                [':list_id' => $listId, ':key' => $def->table_name, ':values'=>json_encode(self::$translations[$def->table_name])]);
                $moduleId = Mbase2Database::fetchField($res,'id');
            }

            if ($def->type === 'referenced_tables') {

                $schema = 'mb2data';

                $p = [
                    ':id' => $moduleId,
                    ':key' => isset($def->key) ? $def->key : 'id',
                    ':schema' => isset($def->schema) ? $def->schema : $schema,
                    ':label_key' => isset($def->label_key) ? $def->label_key : 'label',
                    ':select_from' => isset($def->select_from) ? $def->select_from : null,
                    ':static' => $def->static === true
                ];

                try{
                    Mbase2Database::query("INSERT INTO mbase2.referenced_tables (id, pkey, schema, label_key, select_from, static) values (
                        :id,
                        :key,
                        :schema,
                        :label_key,
                        :select_from,
                        :static
                    )
                    ON CONFLICT (id) DO 
                    UPDATE SET pkey=:key, schema=:schema, select_from=:select_from, label_key=:label_key, static=:static
                    ",$p);
                }
                catch (Exception $e) {
                    echo $e->getMessage();
                }
            }

            return $moduleId;
        }

        static function prenosTabel($def, $prenos, $static = true) {
            
            foreach ($prenos as $source => $tdef) {
                $target = $tdef['target'];

                echo "Prenos: ".$source." => ".$target."\n";

                list($schema, $target) = self::explodeTableName($target);

                if ($static && !isset($def[$target])) continue;
                if (!$static && isset($def[$target])) continue;

                $addModuleVariables = false;
                if ($static) {
                    $addModuleVariables = true; //static tables usually don't have variables defined - I define them here to get the table created int the updateSchema function
                }
                
                $moduleId = self::tableDefinitions($def, $target, $addModuleVariables);
                Mbase2Database::updateSchema($moduleId, $schema);

                $cmap = $tdef['cmap'];
                $ctnames = [];
                $csnames = [];
                foreach ($cmap as $csource => $ctarget) {
                    $csnames[] = $csource;
                    $ctnames[] = $ctarget;
                }
                
                $ctnames = implode(',', $ctnames);
                $csnames = implode(',', $csnames);

                $from = isset($tdef['sql']) ? "({$tdef['sql']})" : "public.$source";

                $sql = "INSERT INTO $schema.$target ($ctnames) SELECT 
                    $csnames FROM $from a";

                try {
                    Mbase2Database::query($sql);
                }
                catch (Exception $e) {
                    self::$exceptions[] = [
                        'msg' => $e->getMessage(),
                        'sql' => $sql
                    ];
                }
            }

        }

        //mb2_code_list_key_id(mb2_get_code_list_id('data_types'), 'integer')
        static function getCodeListId($labelKey, $insertIfNotExists=false) {
            $p = [':label_key' => $labelKey];
            $res = Mbase2Database::query("SELECT id from mbase2.code_lists WHERE label_key=:label_key",
                $p
            );

            $clistId = Mbase2Database::fetchField($res, 'id');

            if ($clistId === false && $insertIfNotExists === true) {
                $res = Mbase2Database::query("INSERT INTO mbase2.code_lists (label_key) VALUES (:label_key) RETURNING id", $p);
                Mbase2Database::query("INSERT INTO mbase2.code_list_options (list_id, key) 
                SELECT id, '$labelKey' FROM mbase2.code_lists WHERE label_key='code_lists'
                ");
                $clistId = Mbase2Database::fetchField($res, 'id');
            }

            return $clistId;
        }

        static function getCodeListKeyId($listId, $key) {
            $res = Mbase2Database::query("SELECT id from mbase2.code_list_options WHERE 
                list_id = :list_id AND key = :key",
                [
                    ':list_id' => $listId,
                    ':key' => $key
                ]
            );

            return Mbase2Database::fetchField($res, 'id');
        }

        static function getKeyId($listKey, $key, $insertIfNotExists = false) {
            $res = Mbase2Database::query("SELECT clv.id FROM 
                mbase2.code_list_options clv, 
                mbase2.code_lists cl
                WHERE
                clv.list_id = cl.id AND 
                clv.key = :key AND
                cl.label_key = :label_key",
                [
                    ':label_key' => $listKey,
                    ':key' => $key
                ]
            );

            $id = Mbase2Database::fetchField($res, 'id');

            if ($id === false && $insertIfNotExists === true) {
                $clistId = self::getCodeListId($listKey, true);
                
                $res = Mbase2Database::query("INSERT INTO mbase2.code_list_options (list_id, key, values) VALUES
                (:list_id, :key, :values) RETURNING id", [':list_id' => $clistId, ':key'=>$key,':values' => json_encode(self::$translations[$key])]);

                $id = Mbase2Database::fetchField($res, 'id');
            }

            return $id;
        }

        public static function explodeTableName($tname) {
            $a = explode('.', $tname);
            if (count($a)>1) {
                return $a;
            }
            else if (count($a) === 1) {
                return ['public', $a[0]];
            }
            return [];
        }

        public static function tableDefinitions($def, $tname, $addModuleVariables = true) {
            $def = (object)$def[$tname];
            $def->table_name = $tname;
        
            $moduleId = self::tableDefinition($def);

            if ($def->type === 'modules') {
                try {
                    Mbase2Database::query("INSERT INTO mbase2.modules (id) values (:id)",[':id'=>$moduleId]);
                }
                catch (Exception $e) {
                    ;
                }
            }

            if (!$addModuleVariables) return $moduleId;

            foreach ($def->variables as $inx => $variable) {
                $variable = (object)$variable;
                $dataTypeId = self::getKeyId('data_types', $variable->key_data_type_id, true);
                $nameId = self::getKeyId('variables', $variable->key_name_id, true);
                $ref = null;
                
                if ($variable->key_data_type_id === 'table_reference' || $variable->key_data_type_id === 'table_reference_array') {
                    $ref = self::getKeyId('referenced_tables', $variable->ref, true);
                }
                else if ($variable->key_data_type_id === 'code_list_reference' || $variable->key_data_type_id === 'code_list_reference_array') {
                    $ref = self::getKeyId('code_lists', $variable->ref, true);
                }

                $required = $variable->required === true;

                $params=[
                    ':data_type_id'=>$dataTypeId,
                    ':name_id' => $nameId,
                    ':ref' => $ref,
                    ':module_id' =>$moduleId,
                    ':required' => $required,
                    ':unique_constraint' => $variable->unique_constraint===true,
                    ':weight' => $inx,
                    ':pattern_id' => isset(self::$patterns[$variable->key_name_id]) ? self::$patterns[$variable->key_name_id] : null,
                    ':form_data' => isset($variable->form_data) ? $variable->form_data : false
                ];

                Mbase2Database::query("INSERT INTO mbase2.module_variables 
                (data_type_id, name_id, ref, module_id, required, unique_constraint, weight, pattern_id, form_data) VALUES (
                    :data_type_id, :name_id, :ref, :module_id, :required,:unique_constraint,:weight,:pattern_id, :form_data
                ) ON CONFLICT (name_id, module_id) DO 
                UPDATE SET data_type_id=:data_type_id, ref=:ref, required = :required, unique_constraint = :unique_constraint, weight=:weight, pattern_id=:pattern_id, form_data=:form_data;",
                    $params
                );
            }

            if ($def->static !== true) {
                Mbase2Database::updateSchema($moduleId);

                foreach ($def->variables as $inx => $variable) {
                    $variable = (object)$variable;
                    $column = $variable->key_name_id;

                    if (isset($variable->default)) {
                        Mbase2Database::query(
                            "
                                ALTER TABLE mb2data.$tname ALTER COLUMN $column DROP DEFAULT;
                                ALTER TABLE mb2data.$tname ALTER COLUMN $column SET DEFAULT {$variable->default};
                            ");
                    }
                    else if (isset($variable->check)) {
                        $constraint = "check_$column";
                        //ALTER TABLE mb2data.dmg_claims ADD CHECK
                        Mbase2Database::query("ALTER TABLE mb2data.$tname DROP CONSTRAINT IF EXISTS $constraint;");
                        Mbase2Database::query("ALTER TABLE mb2data.$tname ADD CONSTRAINT $constraint {$variable->check}");
                    }
                }
            }

            if (isset($def->constraints)) {
                foreach($def->constraints as $constraint) {
                    echo "$constraint\n";
                    Mbase2Database::query($constraint);
                }
            }

            return $moduleId;
        }

        static function addPatterns() {
            foreach (self::$patterns as $variableKey => $key) {
                self::$patterns[$variableKey] = self::getKeyId('validation_patterns', $key, true);
            }
        }

        static function codeLists() {
            foreach (self::$codeListValues as $labelKey => $sqls) {
                $labelId = self::getCodeListId($labelKey, true);
                Mbase2Database::query($sqls);
            }
        }

        static function sifranti() {
            require_once(__DIR__."/assets/dmg_trees.php");

            $labelId = self::getCodeListId('skodni_objekti_options', true);
            $labelIdZGS = self::getCodeListId('dmg_objects_options', true);
            dmg_trees_import('skodni_objekti.csv', 'dmg_skodni_objekti', 2, "", 3, 1, true, $labelId, $labelIdZGS);

            $labelId = self::getCodeListId('culprit_options', true);
            $labelIdZGS = self::getCodeListId('species', true);
            dmg_trees_import('povzrocitelji.csv','dmg_culprits',1,null,2,null,false, $labelId, $labelIdZGS);

            cenik();
            deputies();
        }

        static function addTableDefinitions($tables) {
            foreach ($tables as $tname => $def) {
                if (isset($def['clone'])) {
                    $required = $def['clone']['required'];
                    $ntname = $def['clone']['tname'];
                    $type = $def['clone']['type'];
                    $ndef = $def;
                    foreach ($ndef['variables'] as &$variable) {
                        if (in_array($variable['key_name_id'], $required)) {
                            $variable['required'] = true;
                        }
                        else {
                            $variable['required'] = false;
                        }
                    }
                    unset($variable);
                    unset($ndef['clone']);
                    if (is_null($type) && isset($ndef['type'])) {
                        unset($ndef['type']);
                    }
                    
                    $tables[$ntname] = $ndef;
                }    
            }

            foreach ($tables as $tname => $def) {
                $mid = self::tableDefinitions($tables, $tname);
                echo $mid." ".$tname."\n";
            }
        }

        public static function clearSchema($module) {
            require_once(__DIR__."/schemas/$module.php");

            schemas\Initialize::clear();
            return [];

        }

        public static function resetSchema($module) {
            Mbase2Database::query("
                set schema 'mbase2';
                SET search_path TO public, mbase2;
            ");
            $moduleId = Mbase2Database::query("SELECT mb2_code_list_key_id(mb2_get_code_list_id('modules'),:module)",[':module' => $module])[0]['mb2_code_list_key_id'];
            echo "RESET: $moduleId $module\n";
            Mbase2Database::query("UPDATE mbase2.modules SET patch_id=-1 WHERE id=:moduleId", [':moduleId' => $moduleId]);
        }

        public static function initSchema($module, $patchId = null) {
            //http://localhost:8080/api/mbase2/schema/init/dmg
            
            require_once(__DIR__."/schemas/$module.php");

            schemas\Initialize::run($module, $patchId);
            return [];

            schemas\Functions::dmg_file_uploads();

            self::$translations = schemas\TableDefinitions::$translations;
            self::$patterns = schemas\TableDefinitions::$patterns;
            self::$codeListValues = schemas\TableDefinitions::$codeListValues;

            foreach (schemas\TableDefinitions::$additionalBefore as $sql) {
                try {
                    Mbase2Database::query($sql);
                }
                catch (Exception $e) {
                    self::$exceptions[] = [
                        'msg' => $e->getMessage(),
                        'sql' => $sql
                    ];
                }
            }

            self::addPatterns();

            self::codeLists();

            self::sifranti();

            self::prenosTabel(schemas\TableDefinitions::$tablesToBeMigrated, schemas\TableDefinitions::$prenos);
            
            $tables = schemas\TableDefinitions::$tables;

            self::addTableDefinitions($tables);
            
            self::prenosTabel(schemas\TableDefinitions::$tablesToBeMigrated, schemas\TableDefinitions::$prenos, false);
            
            foreach ($tables as $tname => $def) {
                if (isset($def['view'])) {
                    Mbase2Database::query($def['view']);
                }
            }

            foreach (schemas\TableDefinitions::$additional as $sql) {
                try {
                    Mbase2Database::query($sql);
                }
                catch (Exception $e) {
                    self::$exceptions[] = [
                        'msg' => $e->getMessage(),
                        'sql' => $sql
                    ];
                }
            }

            foreach (self::$translations as $key => $values) {
                Mbase2Database::query("UPDATE mbase2.code_list_options SET values = :values WHERE key = :key", [':values' => json_encode($values), ':key'=>$key]);
            }

            Mbase2Database::query("update mbase2.code_list_options as clo
                set list_key = label_key
                from mbase2.code_lists cl
                where clo.list_id = cl.id");

            Mbase2Database::query("update mbase2.code_list_options as clo
                set list_key_id = cl.id
                from (select id, key from mbase2.code_list_options where list_key='code_lists') cl
                where clo.list_key = cl.key");
            
            Mbase2Database::query("BEGIN");
            Mbase2Database::query("set schema 'mbase2'");
            Mbase2Database::query("REFRESH MATERIALIZED VIEW mbase2.code_list_options_fkeys");
            Mbase2Database::query("COMMIT");

            print_r(self::$exceptions);
        }
    }
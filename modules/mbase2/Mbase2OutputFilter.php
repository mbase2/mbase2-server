<?php

class Mbase2OutputFilter {

    function __construct($input, &$result, $module, $tname)
    {
      $this->input = $input;
      $this->result = &$result;
      $this->module = $module;
      $this->tname = $tname;
      $this->filter();
    }

    private function applyFilter() {

      if (Mbase2Drupal::isAdmin($this->module)) return false;

      if ($this->module === 'dmg' && in_array($this->tname,['dmg_claims', 'dmg_view_deputies'])) {
        if (isset($this->input[':__select'])) return false; //this is needed to reconstruct claim_id when viewing claims from other OE
        return true;
      }

      return false;
    }

    private function filter() {
      if (!$this->applyFilter()) return;
      
      require_once(__DIR__.'/Mbase2Utils.php');
      $_oe_id = Mbase2Utils::dmg_get_deputy_oeid();
      if (empty($_oe_id)) throw new Exception('You have to be a deputy to access this data.');
      
      $oeid = str_pad($_oe_id, 2, '0', STR_PAD_LEFT);

      if ($this->tname === 'dmg_claims') {
        foreach($this->result as &$row) {
          $claimId = $row['_claim_id'];
          if (!empty($claimId)) {
            if (substr($claimId, 0, 2) !== $oeid) {
              $row['_affectees'] = '["*******"]';

              $row['__access'] = 404;

              $objekti = json_decode($row['_skodni_objekti']);
              foreach ($objekti as &$objekt) {
                $objekt->nodes[0]->text = '<b>Oškodovanec</b>&nbsp**********';
                $data = json_decode($objekt->data);
                $data->affectee = null;
                $objekt->data = json_encode($data);
              }
              unset($objekt);
              $row['_skodni_objekti'] = json_encode($objekti);
            }
          }
        }
        unset($row);
      }
      else if ($this->tname === 'dmg_view_deputies') {
        $filtered = [];
        foreach($this->result as $row) {
          if ($row['_oe_id' ] == $_oe_id) {
            $filtered[] = $row;
          }
        }
        $this->result = $filtered;
      }
    }
}
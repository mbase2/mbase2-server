<?php

/*
mb2data.cnt_monitoringArray
              
(
    [0] => Array
        (
            [id] => 5
            [_vrsta_opazovanja] => 17366
            [_start_date] => 2021-08-10
            [_end_date] => 2021-08-10
            [_notes] => 
            [monitoring_data] => 
        )

)

[8] => Array
        (
            [id] => 9
            [_location] => 0101000020E6100000567F8A509F652D40B4A18C6F87E84640
            [_location_data] => {"_location": {"lat": 45.8166331707647, "lon": 14.6984810990395, "spatial_request_result": {"oe_ime": "Kočevje", "lov_ime": "DOBREPOLJE", "luo_ime": "Kočevsko Belokranjsko", "ob_uime": "Dobrepolje"}}}
            [_local_name] => Vrhunska voda
            [_cnt_permanent_spot_code] => 061-047
            [_cnt_spot_skrbnik] => 
            [_cnt_active] => 1
        )

*/

class Mbase2PostInsert {

    function __construct($tname, $result) {
        $this->tname = $tname;
        $this->result = $result;
        $this->process();
    }

    private function process() {
        if ($this->tname === 'mb2data.cnt_monitoring') {
            $this->cntMonitoring($this->result);
        }
    }

    private function cntMonitoring() {
        $vrstaMonitoringaId = @$this->result[0]['_vrsta_opazovanja'];
        if (empty($vrstaMonitoringaId)) return;

        $clrow = Mbase2Database::query("SELECT * FROM mbase2.code_list_options WHERE id=:id",[':id' => $vrstaMonitoringaId]);

        if (isset($clrow[0]) && $clrow[0]['key'] === 'stalna števna mesta') {
            $res = Mbase2Database::query("SELECT * FROM mb2data.cnt_permanent_spots WHERE _cnt_active=true");
            $monitoringId = $this->result[0]['id'];

            $values = "($monitoringId, ".implode("),($monitoringId,",array_column($res,'id')).')';

            Mbase2Database::query("INSERT INTO mb2data.cnt_observation_reports (monitoring_id, _cnt_location_id) values $values");
        }
        else {
            $res = Mbase2Database::query("SELECT * FROM mbase2.view_lov");
            $monitoringId = $this->result[0]['id'];
            $values = "($monitoringId, ".implode("),($monitoringId,",array_column($res,'koda')).')';
            Mbase2Database::query("INSERT INTO mb2data.cnt_estimations (monitoring_id, _cnt_lov_koda) values $values");
        }
        
    }
}
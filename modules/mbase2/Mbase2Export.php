<?php

    require 'vendor/autoload.php';

    use PhpOffice\PhpSpreadsheet\Spreadsheet;
    use PhpOffice\PhpSpreadsheet\IOFactory;

    use proj4php\Proj4php;
    use proj4php\Proj;
    use proj4php\Point;

    final class Mbase2Export {

        static private function outputToExcel($header, $data, $filename='mbase-export') {
            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();

            $cinx = 1;
            $rinx = 1;
            foreach ($header as $value) {
                $sheet->setCellValueByColumnAndRow($cinx,$rinx, $value);
                $cinx++;
            }

            $rinx = 2;
            foreach($data as $dataRow) {
                $cinx = 1;
                foreach ($header as $key) {
                    $value = $dataRow[$key];
                    $sheet->setCellValueByColumnAndRow($cinx,$rinx, $value);
                    $cinx++;
                }
                $rinx++;
            }

            $spreadsheet->setActiveSheetIndex(0);

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="'.$filename.'.xlsx"');
            header('Cache-Control: max-age=0');

            $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');

            $writer->save('php://output');
        }

        static private function dmg_groupObjectsByAffectee($damageObjects) {
            $result = [];
            foreach ($damageObjects as $obj) {
                $data = $obj->data;
                $affecteeId = $data->affectee;
                if (!isset($result[$affecteeId])) {
                    $result[$affecteeId] = [];
                }
                $result[$affecteeId][$obj->oid] = $obj;
            }
            return $result;
        }

        static private function dmg_damageObjectPrices(&$damageObjectsByAffectees, $claim_id, $agreements) {

            //foreach($agreements as $aid => $data) {
            foreach($damageObjectsByAffectees as $affecteeId => &$damageObjects) {
                $agreementData = isset($agreements[$claim_id.'_'.$affecteeId]) ? $agreements[$claim_id.'_'.$affecteeId] : [];
                
                foreach($agreementData->_odskodnina as $row) {
                    $oid = $row[0]->oid;
                    if (isset($damageObjects[$oid])) {
                        $damageObjects[$oid]->price = $row[5][0];
                    }
                    //_odskodnina_additional
                }
            }
        }

        static private function getCodeListValue(&$clist, $id) {
            if (empty($id)) return '';
            if (!isset($clist[$id])) return '';
            $values = $clist[$id]['values'];
            if (!empty($values)) {
                if (is_string($values)) {
                    $values = json_decode($values);
                    $clist[$id]['values'] = $values;
                }
                
                if (isset($values->sl)) {
                    return $values->sl;
                }
            }

            return $clist[$id]['key'];
        }

        static private function getUserName($users, $id) {
            if (empty($id)) return '';
            if (!isset($users[$id])) return '';
            $user = $users[$id];
            return $user['_ime'].' '.$user['_priimek'].' ('.$user['_uname'].')';
        }

        static private function selectWithSpecify($clist, $row, $edata, $key) {
            return self::getCodeListValue($clist, $row[$key]).(isset($edata->$key) ? " ({$edata->$key})" : '');
        }

        static private function interventionsExport() {
            $users = Mbase2Database::query("SELECT * FROM mbase2._users");
            $users = array_column($users, null, '_uid');

            $clist = Mbase2Database::query("SELECT * FROM mbase2.code_list_options");
            $clist = array_column($clist, null, 'id');

            $proj4 = new Proj4php();
            $proj4->addDef("EPSG:3912",'+proj=tmerc +lat_0=0 +lon_0=15 +k=0.9999 +x_0=500000 +y_0=-5000000 +ellps=bessel +towgs84=409.545,72.164,486.872,3.085957,5.469110,-11.020289,17.919665 +units=m +no_defs');

            $projFrom = new Proj('EPSG:4326', $proj4);
            $projTo = new Proj('EPSG:3912', $proj4);

            $output = [];
            ////////////////////////////////////////////////////
            $interventions = Mbase2Database::query("SELECT * from mb2data.interventions");
            $interventionsEventsRaw = Mbase2Database::query("SELECT * from mb2data.interventions_events ORDER by intervention_id");

            $interventionsEvents = [];
            foreach ($interventionsEventsRaw as $ier) {
                $interventionId = $ier['intervention_id'];
                if (!isset($interventionsEvents[$interventionId])) $interventionsEvents[$interventionId] = [];
                $interventionsEvents[$interventionId][] = $ier;
            }
            
            foreach ($interventions as $intervention) {
                $outputRow = [];
                $outputRow['Živalska vrsta'] = self::getCodeListValue($clist, $intervention['_species_name']);
                $data = json_decode($intervention['_data']);
                $spatialData = $data->_location;
                $spatialRequestResult = $spatialData->spatial_request_result;
                
                $outputRow['Območna enota ZGS'] = $spatialRequestResult->oe_ime;
                $outputRow['Občina'] = $spatialRequestResult->ob_uime;
                $outputRow['LUO'] = $spatialRequestResult->luo_ime;
                $outputRow['Lovišče'] = $spatialRequestResult->lov_ime;

                $outputRow['Opombe'] = $intervention['notes'];

                if (isset($interventionsEvents[$intervention['id']])) {
                    foreach ($interventionsEvents[$intervention['id']] as $einx => $e) {
                        $edata = json_decode($e['_data']);

                        $outputRow["Klic ($einx)"] = self::selectWithSpecify($clist, $e, $edata, 'intervention_caller');
                        $outputRow["Vzrok intervencije ($einx)"] = self::selectWithSpecify($clist, $e, $edata, 'intervention_reason');

                        $outputRow["Opis situacije ($einx)"] = $e['situation_notes'];

                        $measures = [];
                        if (!empty($e['intervention_measures'])) {
                            $ims = json_decode($e['intervention_measures']);
                            
                            foreach($ims as $im) {
                                $measure = self::getCodeListValue($clist, $im);
                                $mkey = isset($clist[$im]) ? $clist[$im]['key'] : null;
                                if (!empty($mkey)) {
                                    if (strpos($mkey, '_specify')!==FALSE) {
                                        if (isset($edata->intervention_measures)) {
                                            $measure = $measure." ({$edata->intervention_measures})";
                                        }
                                    }
                                }
                                $measures[] = $measure;
                            }    
                        }
                        
                        $outputRow["Intervencijski ukrepi ($einx)"] = implode(', ', $measures);
                        
                        $outputRow["Izid intervencije ($einx)"] = self::selectWithSpecify($clist, $e, $edata, 'intervention_outcome');

                        $outputRow["Vodja intervencije ($einx)"] = self::getUserName($users, $e['chief_interventor']);

                        $interventorsOut = [];
                        if (!empty($e['interventors'])) {
                            $interventors = json_decode($e['interventors']);
                            foreach ($interventors as $interventor) {
                                $interventorsOut[] = self::getUserName($users, $interventor);
                            }
                        }

                        $outputRow["Člani intervencijske skupine ($einx)"] = implode(', ', $interventorsOut);

                        $outputRow["Datum in čas klica ($einx)"] = $e['intervention_call_timestamp']; 
                        $outputRow["Datum in čas začetka ($einx)"] = $e['intervention_start_timestamp']; 
                        $outputRow["Datum in čas konca ($einx)"] = $e['intervention_end_timestamp']; 

                        $outputRow["Opombe ($einx)"] = $e['notes'];
                    }
                }

                $outputRow['Uporabnik'] = self::getUserName($users, $intervention['_uname']);
                $outputRow['Datum in čas prvega vpisa'] = $intervention['date_record_created'];
                $outputRow['Datum in čas zadnje spremembe'] = $intervention['date_record_modified'];

                $output[] = $outputRow;
            }

            //////////////////////////////////////////////

            $coloumnCount = 0;
            $header = [];

            foreach($output as $outputRow) {
                $cheader = array_keys($outputRow);
                if (count($cheader) > $coloumnCount) {
                    $coloumnCount = count($cheader);
                    $header = $cheader;
                }
            }

            self::outputToExcel($header, $output);
        }
       
        static private function dmgExport($oeid = null) {
            $w='';

            if (!is_null($oeid)) $w="AND _claim_id LIKE '$oeid/%'";

            $claimsRes = Mbase2Database::query("SELECT * FROM mb2data.dmg_claims WHERE _claim_status=1 $w");

            $agreementsRes = Mbase2Database::query("SELECT * from mb2data.dmg_agreements WHERE _completed=true");
            $agreements = [];
            foreach($agreementsRes as $r) {
                $agreements[$r['_claim_id'].'_'.$r['_affectee']] = json_decode($r['_data']);
            }

            $users = Mbase2Database::query("SELECT * FROM mbase2._users");
            $users = array_column($users, null, '_uid');

            $clist = Mbase2Database::query("SELECT * FROM mbase2.code_list_options");
            $clist = array_column($clist, null, 'id');

            $affectees = Mbase2Database::query("SELECT id, _full_name || ', ' || _street || ' ' || _house_number || ', ' || _post_number as naslov FROM mb2data.dmg_affectees");
            $affectees = array_column($affectees, null, 'id');

            $agreementStatuses = [
                1 => 'Sporazum JE dosežen.',
                2 => 'Sporazum NI dosežen',
                3 => 'Obrazec 2.3 - vloga stranke'
            ];

            $expectedEventDomain = [
                1 => 'DA', 
                2 => 'NE'
            ];

            $proj4 = new Proj4php();
            $proj4->addDef("EPSG:3912",'+proj=tmerc +lat_0=0 +lon_0=15 +k=0.9999 +x_0=500000 +y_0=-5000000 +ellps=bessel +towgs84=409.545,72.164,486.872,3.085957,5.469110,-11.020289,17.919665 +units=m +no_defs');

            $projFrom = new Proj('EPSG:4326', $proj4);
            $projTo = new Proj('EPSG:3912', $proj4);

            $output = [];

            foreach ($claimsRes as $row) {
                $outputRow = [];
                $outputRow['ID_record'] = $row['id'];
                $outputRow['Koda škodnega zahtevka'] = $row['_claim_id'];

                $outputRow['Povezava do zahtevka (URL)'] = 'https://portal.mbase.org/mbase2/modules/dmg/zahtevek?fid='.$row['id'];

                $outputRow['Ali je bil dogodek objektivno pričakovan?'] = $expectedEventDomain[$row['_expected_event']];

                $spatialData = json_decode($row['_claim_location_data']);
                $spatialRequestResult = $spatialData->spatial_request_result;
                
                $outputRow['Območna enota ZGS'] = $spatialRequestResult->oe_ime;
                
                $deputy = $users[$row['_deputy']];
                
                $outputRow['Pooblaščenec'] = $deputy['_ime'].' '.$deputy['_priimek'].' ('.$deputy['_uname'].')';
                $outputRow['Občina'] = $spatialRequestResult->ob_uime;
                $outputRow['LUO'] = $spatialRequestResult->luo_ime;
                $outputRow['Lovišče'] = $spatialRequestResult->lov_ime;
                $outputRow['Povzročitelj'] = $clist[$row['_culprit']]['key'];
                $outputRow['Datum ogleda škode'] = $row['_dmg_examination_date'];

                $pointDest = (object)['x' => null, 'y' => null];
                
                try {
                    $pointSrc = new Point($spatialData->lon, $spatialData->lat, $projFrom);
                    $pointDest=$proj4->transform($projTo, $pointSrc);
                } catch (Exception $e) {
                    ;
                }

                $outputRow['Zemlj.širina D48/GK'] = $pointDest->y;
                $outputRow['Zemlj.dolžina D48/GK'] = $pointDest->x;
                $outputRow['Zemlj.širina WGS84'] = $spatialData->lat;
                $outputRow['Zemlj.dolžina WGS84'] = $spatialData->lon;

                $skodniObjektiPoOskodovancih = self::dmg_groupObjectsByAffectee(json_decode($row['_skodni_objekti']));

                self::dmg_damageObjectPrices($skodniObjektiPoOskodovancih, $row['id'], $agreements);

                $oskodovanci = json_decode($row['_affectees']);
                $odskodninaSkupaj = 0.0;

                $naciniVarovanjaPoOskodovancihInSkodnihObjektih = [];

                foreach ($oskodovanci as $inx => $aid) {
                    $ainx = $inx+1;
                    $naciniVarovanjaPoOskodovancihInSkodnihObjektih[$ainx] = [];
                    $outputRow["Oškodovanec ($ainx)"] = $affectees[$aid]['naslov'];
                    $skodniObjektiOskodovanca = isset($skodniObjektiPoOskodovancih[$aid]) ? $skodniObjektiPoOskodovancih[$aid] : [];

                    $agreement = isset($agreements[$row['id'].'_'.$aid]) ? $agreements[$row['id'].'_'.$aid] : null;
                    $statusSporazuma = is_null($agreement) ? '' : $agreementStatuses[$agreement->_status];
                    $kodaStatusa = is_null($agreement) ? null : $agreement->_status;
                    $outputRow["Status sporazuma ($ainx)"] = $statusSporazuma;

                    $price = 0.0;

                    $ooinx = 1;
                    
                    foreach ($skodniObjektiOskodovanca as $oid => $skodniObjekt) {
                        
                        $data = $skodniObjekt->data;
                        
                        $naciniVarovanjaPoOskodovancihInSkodnihObjektih[$ainx][$ooinx] = $data->_nacini_varovanja;

                        $outputRow["Škodni objekt ZGS (oškodovanec: $ainx, objekt: $ooinx)"] = $skodniObjekt->zgsKey;
                        $outputRow["Škodni objekt (oškodovanec: $ainx, objekt: $ooinx)"] = $skodniObjekt->text;
                        $outputRow["Količina (oškodovanec: $ainx, objekt: $ooinx)"] = $data->kolicina;
                        $outputRow["Enota (oškodovanec: $ainx, objekt: $ooinx)"] = $data->enota;

                        if ($kodaStatusa == 1) {
                            $outputRow["Odškodnina (oškodovanec: $ainx, objekt: $ooinx)"] = $skodniObjekt->price;
                            $price = $price + floatval($skodniObjekt->price);
                        }

                        $ooinx++;
                    }

                    if ($kodaStatusa == 1) {
                        $odskodninaDodatno = 0.0;
                        $additional = isset($agreement->_odskodnina_additional) ? $agreement->_odskodnina_additional : [];

                        foreach($additional as $arow) {
                            $odskodninaDodatno = $odskodninaDodatno + floatval($arow->row[4]);
                        }

                        $outputRow["Odškodnina dodatno (oškodovanec: $ainx)"] = $odskodninaDodatno;
                        $outputRow["Odškodnina skupaj (oškodovanec: $ainx)"] = $odskodninaDodatno + $price;

                        $odskodninaSkupaj = $odskodninaSkupaj + $odskodninaDodatno + $price;
                    }
                }
                $outputRow["Odškodnina skupaj"] = $odskodninaSkupaj;

                foreach ($naciniVarovanjaPoOskodovancihInSkodnihObjektih as $ainx => $naciniVarovanjaPoSkodnihObjektihOskodovanca) {
                    foreach ($naciniVarovanjaPoSkodnihObjektihOskodovanca as $ooinx => $naciniVarovanjaPoSkodnemObjektu) {
                        $nvinx = 1;
                        foreach ($naciniVarovanjaPoSkodnemObjektu as $nacinVarovanja) {
                            $imeVarovanja = array_keys(get_object_vars($nacinVarovanja))[0];
                            $outputRow["Varovanje (oškodovanec: $ainx, objekt: $ooinx, varovanje: $nvinx)"] = $imeVarovanja;
                            $outputRow["Lastnosti varovanja (oškodovanec: $ainx, objekt: $ooinx, varovanje: $nvinx)"] = json_encode($nacinVarovanja->$imeVarovanja);
                            $nvinx++;
                        }
                    }
                }
                
                $outputRow["Odvzet vzorec za DNK (DA/NE)"] = count(json_decode($row['_genetic_samples'])) > 0 ? 'DA' : 'NE';

                $outputRow["Opombe"] = $row['_deputy_notes'];

                $output[] = $outputRow;
            }

            $coloumnCount = 0;
            $header = [];

            foreach($output as $outputRow) {
                $cheader = array_keys($outputRow);
                if (count($cheader) > $coloumnCount) {
                    $coloumnCount = count($cheader);
                    $header = $cheader;
                }
            }

            self::outputToExcel($header, $output);
        }

        static function exportModuleData($module) {
            if ($module === 'dmg') {
                if (Mbase2Drupal::isAdmin($module)) {
                    self::dmgExport();
                }
                else {
                    require_once(__DIR__.'/Mbase2Utils.php');
                    $_oe_id = Mbase2Utils::dmg_get_deputy_oeid();
                    if (empty($_oe_id)) throw new Exception('You have to be a deputy to access this data.');
                    self::dmgExport(str_pad($_oe_id, 2, '0', STR_PAD_LEFT));
                }
            }
            else if ($module === 'interventions') {
                self::interventionsExport();
            }
        }
    }
<?php

final class Mbase2UAC {

    public static function delete($tname, $id=null) {
        if ($tname === 'mb2data.dmg_claims') {
            $res = Mbase2Database::query("SELECT _uid,_claim_status from mb2data.dmg_claims WHERE id=:id",[':id' => $id]);
            $uid = Mbase2Database::fetchField($res,'_uid');

            if ($uid == Mbase2Drupal::getCurrentUserData()->uid) {
                return true;
            }
        }
        else {
            
            $uid = '_uname';
            if ($tname === 'mb2data.interventions_events') $uid = '_uid';   //some tables store user id in _uid field and some in _uname

            $res = Mbase2Database::query("SELECT $uid from $tname WHERE id=:id",[':id' => $id]);
            $uid = Mbase2Database::fetchField($res,$uid);

            if ($uid == Mbase2Drupal::getCurrentUserData()->uid) {
                return true;
            }
        }

        throw new Exception('Insufficient privilege to delete the data.');
    }

    public static function select($tname) {
        $roles = Mbase2Drupal::currentUserRoles();

        if (strpos($tname, 'mb2data.') === FALSE) return true;

        if (isset($roles['administrator'])) return true;

        $startPos = strlen('mb2data.');

        $module = substr($tname, $startPos, strpos($tname,'_', $startPos) - $startPos);

        if (!empty($module)) $module = "mbase2_$module";

        $len = strlen($module);

        foreach (array_keys($roles) as $role) {
            if (substr($role, 0, $len) === $module) return true;
        }

        throw new Exception("Insufficient privilege to select the data.");
    }
}
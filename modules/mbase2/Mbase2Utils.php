<?php

    final class Mbase2Utils {

        static function SQLforeignKeyConstraint($sourceSchemaName, $sourceTableName, $sourceFieldName, $targetSchemaName, $targetTableName, $targetFieldName) {
            return "ALTER TABLE $sourceSchemaName.$sourceTableName
            ADD CONSTRAINT {$sourceTableName}_{$sourceFieldName}_fkey
            FOREIGN KEY ($sourceFieldName) REFERENCES $targetSchemaName.$targetTableName ($targetFieldName)";
        }

        static function SQLfunction_sync_date_modified() {
            return 'CREATE OR REPLACE FUNCTION sync_date_modified() RETURNS trigger AS $$
            BEGIN
            NEW.date_record_modified := NOW()::timestamp;
            RETURN NEW;
            END;
            $$ LANGUAGE plpgsql;';
        }


        static function SQLfunction_sync_date_created() {
            return 'CREATE OR REPLACE FUNCTION sync_date_created() RETURNS trigger AS $$
            BEGIN
            NEW.date_record_created := NOW()::timestamp;
            NEW.date_record_modified := NEW.date_record_created;
            RETURN NEW;
            END;
            $$ LANGUAGE plpgsql;';
        }

        static function SQLrequiredConstraint($schema, $tname, $cname) {
            return "ALTER TABLE $schema.$tname ALTER COLUMN $cname SET NOT NULL";
        }

        static function SQLuniqueConstraint($schema, $tname, $cname) {
            return "ALTER TABLE $schema.$tname ADD CONSTRAINT {$tname}_{$cname}_unique_constraint UNIQUE ($cname)";
        }

        static function SQLtrigger_sync_date_modified($schemaName, $tableName) {
            return "CREATE TRIGGER
            ${schemaName}_${tableName}_modified
            BEFORE UPDATE ON
            $schemaName.$tableName
            FOR EACH ROW EXECUTE PROCEDURE
            sync_date_modified();";
        }

        static function SQLtrigger_sync_date_created($schemaName, $tableName) {
            return "CREATE TRIGGER
            ${schemaName}_${tableName}_created
            BEFORE INSERT ON
            $schemaName.$tableName
            FOR EACH ROW EXECUTE PROCEDURE
            sync_date_created();";
        }

        static function getModuleNameFromTableName($tname) {
            $pos = strpos($tname,'_');

            if ($pos === false) return null;

            $moduleName = substr($tname, 0, $pos);

            $pos = strpos($moduleName,'.');

            if ($pos === false) return $moduleName;

            return substr($moduleName, $pos+1);
        }

        function get_tree($args) {
            global $cenik;
            $tname = $args[count($args)-1];

            $res = Mbase2Database::query("SELECT * FROM $tname order by id");

            $cenik = false;

            if (isset($res[0]) && isset($res[0]['cenik'])) {
                $cenik = true;
            }
            
            /**
             * https://stackoverflow.com/questions/8587341/recursive-function-to-generate-multidimensional-array-from-database-result/8587437#8587437
             */
            function buildTree($rows, $parentId = null) {
                global $cenik;
                $branch = array();
            
                foreach ($rows as $row) {
                    $parent = $row['key_parent'];
                    $item = $row['key_item'];
                    if ($parent === $parentId) {
                        $nodes = buildTree($rows, $row['key_item']);
                        $node = ['text' => $item];
                        
                        if (isset($row['_item_id'])) {
                            $node['_item_id'] = $row['_item_id'];
                        }

                        if (isset($row['_mbase2_id'])) {
                            $node['_mbase2_id'] = $row['_mbase2_id'];
                        }

                        if (isset($row['key_mbase2'])) {
                            $node['zgsKey'] = $row['key_mbase2'];
                        }

                        if (!empty($nodes)) {
                            $node['nodes'] = $nodes;
                            $node['selectable'] = false;
                        }
                        else {
                            $node['icon'] = 'fa fa-file-o';
                            if ($cenik) {
                                if (isset($row['cenik'])) {
                                    $node['cenik'] = $row['cenik'];
                                }

                                if (isset($row['navedi'])) {
                                    $node['navedi'] = $row['navedi'];
                                }
                            }
                        }
                        $branch[] = $node;
                    }
                }
            
                return $branch;
            }
            
            $tree = buildTree($res);

            return $tree;
        }

        static function createIndirectClaim($id) {
            $id = intval($id);
            
            Mbase2Database::query("BEGIN");

            $res = null;
            
            try {
                $result = Mbase2Database::query("UPDATE mb2data.dmg_claims SET __indirect_counter=__indirect_counter+1 WHERE id=:id AND _claim_status > 0 RETURNING *", [':id'=>$id]);

                if (empty($result) || count($result) > 1) {
                    throw new Exception('Unable to create indirect claim with the provided ID.');
                }
    
                $affectees = json_decode($result[0]['_affectees']);
                
                $result2 = Mbase2Database::query("SELECT count(*) as cnt FROM mb2data.dmg_agreements WHERE _claim_id = :id AND _completed=true", [':id'=>$id]);
    
                if (empty($result2) || $result2[0]['cnt'] != count($affectees)) {
                    throw new Exception('Indirect claim is only possible with fully completed claims (all agreements resolved)');
                }
    
                $input = $result[0];
                unset($input['id']);
                $input['_claim_id'] = $input['_claim_id'].'/'.$input['__indirect_counter'];
                $input['_uid'] = true;
                $input['_skodni_objekti'] = null;
                $input['_claim_status'] = 0;
    
                //add : to params keys
                $params = [];
    
                foreach ($input as $key => $value) {
                    $params[':'.$key] = $value;
                }

                $res = Mbase2Database::insert('mb2data.dmg_claims', $params);
            }
            catch (Exception $e) {
                Mbase2Database::query("ROLLBACK");
                throw new Exception($e->getMessage());  
            }
    
            Mbase2Database::query("COMMIT");
            
            return $res;
        }

        /**
         * Add mbase2_dmg_editor user role to users in the mb2data.dmg_deputies table
         */

        static function dmg_editor_role() {
            $res = Mbase2Database::query("SELECT _uname as uid from mb2data.dmg_deputies where _uname not in (SELECT ur.uid FROM public.users_roles ur, public.role r WHERE r.rid=ur.rid and r.name = 'mbase2_dmg_editor')");

            $role = Mbase2Drupal::getRoleByName('mbase2_dmg_editor');

            $rid = $role->rid;
            
            $rval = [];
            foreach ($res as $r) {
                $rval[] = Mbase2Drupal::addRoleToUser($rid, $r['uid']);
            }

            return $rval;
        }

        public static function dmg_get_deputy_oeid() {
            $res = Mbase2Database::query("SELECT zgs._oe_id as oeid FROM mb2data.dmg_deputies d, mbase2.obmocne_enote_zgs zgs WHERE zgs.id = d._oe_id AND _uname = :uid",[':uid'=>Mbase2Drupal::getCurrentUserData()->uid]);
      
            if (empty($res)) return $res;
      
            return $res[0]['oeid'];
          }
    }
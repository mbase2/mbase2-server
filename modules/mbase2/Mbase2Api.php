<?php
class Mbase2Api {
  private $result_type = PGSQL_ASSOC;
  private $args = [];
  private $json_flags = JSON_NUMERIC_CHECK;
  private $table = '';
  private $result = [];
  private $request_type = 'GET';
  private $input = [];
  private $uid = null;  //user ID
  private $storagePath = [];

  function __construct($request, $query_parameters = [], $options = [], $echo = true, $result_type = PGSQL_ASSOC) {
    
    $storagePath = $options['storagePath'];
    $this->uid = $options['uid'];

    if (empty($storagePath)) $storagePath = [];

    foreach(['public', 'private'] as $pathKey) {
      $this->storagePath[$pathKey] = isset($storagePath[$pathKey]) ? $storagePath[$pathKey] : sys_get_temp_dir();
    }
    
    $this->result_type = $result_type;

    $this->args = $args = explode('/', $request);

    $this->request_type = $_SERVER['REQUEST_METHOD'];

    Mbase2Database::$uid = $this->uid;

    Mbase2Files::$storagePath = $this->storagePath;
    
    try {
      if ($args[0] === 'code_lists') {
        $this->table = 'mbase2.code_lists';
        $this->run();
      }
      if ($args[0] === 'globals') {
        $this->result = [
          'language' => Mbase2Drupal::getCurrentLanguageCode()
        ];
      }
      else if ($args[0] === 'aggregate_defs') {
        $this->table = 'mbase2.aggregate_defs';
        $this->run();
      }
      else if ($args[0] === 'test') {
        if (!Mbase2Drupal::isAdmin()) return;
        var_dump(call_user_func_array([$args[1], $args[2]], json_decode($args[3])));
      }
      else if ($args[0] === 'mbase2utils') {
        require_once(__DIR__.'/Mbase2Utils.php');
        $this->result = call_user_func(["Mbase2Utils", $args[1]],$args);
      }
      else if ($args[0] === 'attributes') {
        $this->table = 'mbase2.attributes';
        $this->run();
      }
      else if ($args[0] === 'export') {
        $module = $args[1];
        
        require_once(__DIR__.'/Mbase2Export.php');
        Mbase2Export::exportModuleData($module);
        
        return;
      }
      else if ($args[0] === 'dmg-indirect-claim') {
        require_once(__DIR__.'/Mbase2Utils.php');
        $this->result = Mbase2Utils::createIndirectClaim($args[1]);
      }
      else if ($args[0] === 'camelot') {
        $this->parseInputParameters();
        require_once(__DIR__.'/Mbase2Camelot.php');
        $camelotId = intval($args[1]);
        $camelot = new Mbase2Camelot($camelotId, $this->input);
        $camelot->import();

        $import = new Mbase2Import(null, null, 'ct', null, $this->uid, $camelotId, $this->input);
        $this->result = $import->errors;
      }
      else if ($args[0] === 'mb2data') {
        $tname = $args[1];
        $this->table = 'mb2data.'.$tname;
        $this->run(1);

        require_once(__DIR__.'/Mbase2OutputFilter.php');

        new Mbase2OutputFilter($this->input, $this->result, 'dmg', $tname);
      }
      else if ($args[0] === 'mbase2') {
        $tname = $args[1];

        if ($this->request_type !== 'GET') {
          throw new Exception("Only GET request is allowed.");
        }
        
        if (!in_array($tname, ['view_lov', 'view_luo', 'view_luo_lov'])) {
          throw new Exception("You are not allowed to access this table.");
        }

        $this->table = 'mbase2.'.$tname;
        $this->run(1);
      }
      else if ($args[0] === 'code_list_options') {
        $this->table = 'mbase2.code_list_options';
        $this->run();
      }
      else if ($args[0] === '_import_batches') {
        $this->table = 'mbase2._import_batches';
        $this->run();
      }
      else if ($args[0] === 'import_errors') {
        $this->table = 'mbase2.import_errors';
        $this->run();
      }
      else if ($args[0] === 'table_reference_values') {
        $this->table = 'mbase2.referenced_tables';
        $this->run();
        
        $this->result = Mbase2Database::getReferencedValues($this->result);
      }
      else if ($args[0] === 'referenced_tables') {
        $this->table = 'mbase2.referenced_tables';
        $this->run();
      }
      else if ($args[0] === 'geom') {
        $this->parseInputParameters();
        $this->result = Mbase2Database::getIntersection($args[1], $this->input);
      }
      else if ($args[0] === 'module_references') {
        $nodef = false; 
        if (isset($args[3]) && $args[3] === 'nodef') {
          $nodef = true;
          unset($args[3]);
        }

        $this->parseInputParameters();

        $numericParameter = ':module_id';
        $keyParameter = ':module_id:code_list_options/key';

        $moduleId = null;

        if (isset($this->input[$numericParameter])) {
          $moduleId = $this->input[$numericParameter];
        }
        else if (isset($this->input[$keyParameter])) {
          $moduleKey = $this->input[$keyParameter];
          $res = Mbase2Database::query("SELECT id from mbase2.code_list_options WHERE key=:moduleKey",[':moduleKey'=>$moduleKey]);
          $moduleId = Mbase2Database::fetchField($res, 'id');
        }

        $language = $this->getRequestArgumentsLanguage();

        $this->result = $this->moduleReferences($moduleId, $nodef, $language);
      }
      else if ($args[0] === 'get_data_source_pages') {
        $this->result = Mbase2Files::get_data_source_pages($args[1]);
      }
      else if ($args[0] === 'get_data_source_page_columns') {
        $this->result = Mbase2Files::get_data_source_page_columns($args[1], $args[2]);
      }
      else if ($args[0] === 'import' && $this->request_type === 'POST') {
        $this->parseInputParameters();
        require_once(__DIR__.'/Mbase2Import.php');
        
        $import = new Mbase2Import($args[1], $args[2], $args[3], $this->input, $this->uid, null, null, isset($args[4]) && $args[4] === 'false' ? false : true);
        $this->result = $import->errors;
      }
      else if ($args[0] === 'template') {
        $this->table = 'mbase2.import_templates';
        $this->run();
      }
      else if ($args[0] === 'module_variables') {
        $this->table = 'mbase2.module_variables';
        $this->run();

        if (in_array($this->request_type,['POST', 'PUT'])) {
          $moduleId = empty($this->result[0]) ? null : intval($this->result[0]['module_id']);
          if (!empty($moduleId)) {
            Mbase2Database::updateSchema($moduleId);
          }
        }

      }
      else if ($args[0] === 'uac') {
        $this->table = 'mb2data.'.$args[1];
        $this->run();
      }
      else if ($args[0] === 'run') {
        if ($args[1] === 'dmg_editor_role') {
          require_once(__DIR__.'/Mbase2Utils.php');
          $this->result = Mbase2Utils::dmg_editor_role();  
        }
      }
      else if ($args[0] === '_users') {
        $this->table = 'mbase2._users';
        $this->run();
      }
      else if ($args[0] === 'modules') {
        $this->table = 'mbase2.modules';
        $this->run();
      }
      else if ($args[0] === 'schema') {
        require_once(__DIR__.'/Mbase2Schema.php');
        if ($args[1] === 'init') {
          $this->result = Mbase2Schema::initSchema($args[2], isset($args[3]) ? $args[3] : null);
        }
        else if ($args[1] === 'reset') {
          $this->result = Mbase2Schema::resetSchema($args[2]);
        }
        else if ($args[1] === 'clear') {
          $this->result = Mbase2Schema::clearSchema($args[2]);
        }
      }
      else if ($args[0] === 'camelot-image-upload' && $this->request_type === 'POST') {
        $this->result = Mbase2Files::fileUploadFromCamelot($this->uid, $args[1], $args[2]);
      }
      else if ($args[0] === 'uploaded-files') {
        $this->result = Mbase2Files::getUploadedFiles($this->uid, $args[1]==='private', $args[2], $args[3], $args[4], $limit);
      }
      else if ($args[0] === 'uploaded-file') {
        
        $id = null;
        $file = null;

        $count = count($args);

        $thumbnail = false;

        if ($count === 3) {
          $id = $args[2];
        }
        else if ($count === 4) {
          $id = $args[2];
          if ($args[3] === 'thumbnail') {
            $thumbnail = true;
          }
        }
        else if ($count > 4) {
          $file = [
            'path' => $args[2].'/'.$args[3].'/'.$args[4]
          ];

          if (isset($args[5])) {
            $file['imageType'] = trim($args[5]);
          }

        }
        else {
          $this->result = null;
          return;
        }

        if (isset($args[5]) && !is_null($file)) $file['type'] = $args[5];

        $this->result = Mbase2Files::getUploadedFile($this->uid, $args[1]==='private', $id, $file, $thumbnail);
        return;
      }
      else if ($args[0] === 'update_schema') {
        $this->result = Mbase2Database::updateSchema(intval($args[1]));
      }
      else if ($args[0] === 'file-upload' && $this->request_type === 'POST') {
        $subfolder = isset($args[2]) ? $args[2] : 'mbase2';
        $this->result = Mbase2Files::fileUpload($this->uid, $args[1], false, true, $subfolder);
      }
      else if ($args[0] === 'file-delete' && $this->request_type === 'DELETE') {
        $this->parseInputParameters();
        //$this->result = Mbase2Files::fileDelete($this->input, $this->uid);
      }
      else if ($args[0] === 'batch') {
        $this->parseInputParameters();
        $urls = json_decode($this->input[':urls']);
        $r = [];
        unset($_GET[':urls']);
        foreach($urls as $url0) {
          $_GET = [];
          $pos = strpos($url0, '?');
          $url = $url0;
          
          if ($pos !== false) {
            $url = substr($url0,0,$pos);
            $keyValues = explode('&', substr($url0, $pos));
            foreach ($keyValues as $keyValue) {
              $pos1 = strpos($keyValue, '=');
              if ($pos1 !== false) {
                $_GET[substr($keyValue,1,$pos1-1)] = substr($keyValue, $pos1+1);
              }
            }
          }

          $api = new Mbase2Api($url,$query_parameters, $options, false);
          $r[] = $api->getResult();
        }
        $this->result = $r;
      }
    }
    catch (Exception $e) {
      http_response_code(400);
      $this->result = ['err' => $e->getMessage() ];
    }

    if ($echo) {
      header("Access-Control-Allow-Origin: *");
      header('Content-Type: application/json; charset=UTF-8');
      header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
      header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, X-Requested-With, Cache-Control");
      echo json_encode($this->result, $this->json_flags);
    }
  }

  /**
   * @param {boolean} $nodef //if nodef then returns only array [module_id => table-name]
   */

  public function moduleReferences($moduleId, $nodef = false, $language = null) {
    
    $ids = [];

    Mbase2Database::moduleReferences($moduleId, $ids);
    
    array_unshift($ids, $moduleId);

    $res = $this->moduleVariables($ids, $language);

    $result = array_flip($ids);
    foreach($res as $r) {
      $result[$r['module_id']] = $r['key_module_id'];
    }

    if ($nodef === false) {

      $result = array_flip($result);

      foreach($result as &$r) {
        $r=[];
      }
      unset($r);

      foreach($res as $r) {
        $result[$r['key_module_id']][] = $r;
      }
    }
    
    return $result;
  }

  /**
   * Helper function for calling Mbase2Database::select('mbase2.module_variables',$p)
   */
  private function moduleVariables($moduleIds = [], $language=null) {
    $p=[];
    if (!empty($moduleIds)) $p=[':module_id' => $moduleIds];
    return Mbase2Database::select('mbase2.module_variables', $p, $language);
  }

  private function getRequestArgumentsLanguage() {
    if ($this->request_type === 'GET') {
      $inx = in_array('language',$this->args);
      if ($inx!==false) return $this->args[$inx+1];
    }
    return null;
  }

  public function getResult() {
    return $this->result;
  }

  private function run($tableNameArgsIndex = 0) {
    require_once(__DIR__.'/Mbase2UAC.php');
    require_once(__DIR__.'/Mbase2Utils.php');
    $this->parseInputParameters();

    $id = null;
    $language = null;

    if (isset($this->args[$tableNameArgsIndex + 1])) {
      if ($this->args[$tableNameArgsIndex + 1] === 'language') {
        $language = @$this->args[$tableNameArgsIndex + 2];
        $id = @$this->args[$tableNameArgsIndex + 3];
      }
      else {
        $id = $this->args[$tableNameArgsIndex + 1];
      }
    }

    $dbFunctionName = null;

    if ($this->request_type === 'GET') {
      Mbase2UAC::select($this->table);
      
      $dbFunctionName = 'select';
      if (!is_null($id)) {
        $this->input[':id'] = $id[0]==='(' ? explode(',', trim($id, "()")) : intval($id);
      }
    }
    else if ($this->request_type === 'POST') {
      $dbFunctionName = 'insert';
    }
    else if ($this->request_type === 'PUT') {
      if (!is_null($id)) {
        $this->input[':id'] = intval($id);
        $dbFunctionName = 'update';
      }
    }
    else if ($this->request_type === 'DELETE') {
      if (!is_null($id)) {
        if (Mbase2Drupal::isAdmin(Mbase2Utils::getModuleNameFromTableName($this->table)) ||
            Mbase2UAC::delete($this->table, $id)
        ) {
          $this->input[':id'] = intval($id);
          $dbFunctionName = 'delete';
        }
      }
    }

    if (!is_null($dbFunctionName)) {
      $dbFunctionParameters = [$this->table, $this->input, $language, $this->result_type];
      $this->result = call_user_func_array(['Mbase2Database', $dbFunctionName], $dbFunctionParameters);
      
      if ($dbFunctionName === 'insert') {
        require_once(__DIR__.'/Mbase2PostInsert.php');
        new Mbase2PostInsert($this->table, $this->result);
      }
    }
    
  }  

  private function parseInputParameters() {
    if (array_key_exists('json_flags', $_GET)) {
      $this->json_flags = intval($_GET['json_flags']);
      unset($_GET['json_flags']);
    }

    if ($this->request_type === 'GET') {
      $input = $_GET;
      foreach($input as $key => $value) {
        if ($key[0] !== ':') {
          unset($input[$key]);
        }
      }
      $this->input = $input;
    }
    else {
      $input = file_get_contents('php://input');
      $this->input = json_decode($input, true);
    }

  }
}
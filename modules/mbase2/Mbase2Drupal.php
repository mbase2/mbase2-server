<?php
/**
 * Here I am collecting Drupal specific functions
 */
    final class Mbase2Drupal {

        public static $storagePath = [];
        
        public static function getCurrentLanguageCode() {
            global $language;

            return $language->language;
        }

        public static function getCurrentUserData() {
            global $user;
            return $user;
        }

        public static function currentUserRoles() {
            global $user;
            $roles = array_flip($user->roles);
            return $roles;
        }

        public static function addRoleToUser($rid, $uid) {
            user_multiple_role_edit(array($uid), 'add_role', $rid);
            return ['uid'=>$uid, 'rid'=>$rid];
        }

        public static function getRoleByName($roleName) {
            return user_role_load_by_name($roleName);
        }

        public static function prepareSubfolderStoragePath($subfolder, $private = true) {
            
            $root = self::$storagePath[$private ? 'private' : 'public'];
            $root = rtrim($root, '/');
            $directory = "$root/$subfolder";  
            if (file_prepare_directory($directory, FILE_CREATE_DIRECTORY) === false){
                throw new Exception('["err":"Directory '.$directory.' not writable"]');
            }
        }

        public static function isAdmin($module = null) {
            global $user;
            $roles = array_flip($user->roles);
            if (isset($roles["administrator"])) return true;

            if (!is_null($module)) {
                if (isset($roles["mbase2_{$module}_admin"])) return true;
            }

            return false;
          }
    }
<?php

require 'vendor/autoload.php';

use Aspera\Spreadsheet\XLSX\Reader;
class Mbase2Import {
    
    private $keyModuleId;
    private $currentRow;
    private $data;
    private $placeholderAttributes;
    public $errors;
        
    function __construct($dsid, $sheetInx, $keyModuleId, $dataDefinitions, $uid, $camelotId = null, $camelotData = null, $selectDefaultModuleVariables = true) {

        //$dataDefinitions = [];
        //$dataDefinitions[$keyModuleId] = json_decode('{"types":{"_species_name":"column","_licence_name":"fixed","event_date":"column","photos":"column","sighting_quantity":"column","trap_station_name":"column","session_name":"column","event_time":"column","sex":"column","individual_name":"column","life_stage":"column","position":"column","hair_trap":"column","notes":"column"},"values":{"_species_name":"42","event_date":"7","photos":"9","sighting_quantity":"26","trap_station_name":"55","session_name":null,"event_time":null,"sex":null,"individual_name":null,"life_stage":null,"position":null,"hair_trap":null,"notes":null,"_licence_name":"10"}}', true);

        $this->keyModuleId = $keyModuleId;

        $res = Mbase2Database::query("SELECT clv.id, clv.key, clv.list_id, clv.list_key label_key FROM mbase2.code_list_options clv WHERE clv.list_key IN ('referenced_tables','validation_patterns', 'data_types', 'variables', 'modules')");
        $clKey = [];
        $clId = [];
        foreach($res as $r) {
            $clKey[$r['label_key'].'.'.$r['key']] = $r['id'];
            $clId[$r['id']] = $r['key'];
        }

        $moduleId = null;
        
        if (isset($clKey["modules.$keyModuleId"])) {
            $moduleId = $clKey["modules.$keyModuleId"];
        }
        else if (isset($clKey["referenced_tables.$keyModuleId"])) {
            $moduleId = $clKey["referenced_tables.$keyModuleId"];
        }

        $selectDefaultSQL = $selectDefaultModuleVariables === true ? "or module_id = (SELECT id from mbase2.code_list_options WHERE key='__default_module')" : "";

        $res = Mbase2Database::query("SELECT * FROM mbase2.module_variables WHERE module_id = :module_id $selectDefaultSQL", [':module_id' => $moduleId]);
        $attributes = [];

        $codeListOptionsIds = [];
        $referencedTableIds = [];

        $preloadImageFileNames = false;

        foreach($res as $r) {
            $dataType = $clId[$r['data_type_id']];

            if ($dataType === 'image' || $dataType === 'image_array') {
                $preloadImageFileNames = true;
            }
            
            if (!empty($r['ref'])) {
                if ($dataType === 'code_list_reference' || $dataType === 'code_list_reference_array') {
                    $codeListOptionsIds[] = $r['ref'];
                }
                else if ($dataType === 'table_reference' || $dataType === 'table_reference_array') {
                    $referencedTableIds[] = $r['ref'];
                }
            }

            $attributes[$clId[$r['name_id']]] = [
                'data_type' => $dataType,
                'required' => $r['required'], //empty($r['required']) ? false : true,
                'unique' => $r['unique_constraint'], //empty($r['unique_constraint']) ? false : true,
                'pattern' => $clId[$r['pattern_id']],
                'ref' => $r['ref']
            ];


        }

        $media = [];

        if ($preloadImageFileNames) {
            $res = Mbase2Database::query("SELECT * FROM mbase2.uploads WHERE uid=:uid and private = true order by record_created",[':uid'=>$uid]);
            foreach ($res as $r) {
                $media[$r['file_name']] = $r['file_hash'];
            }
        }

        $params = [':clv.id' => $codeListOptionsIds];
        $where = Mbase2Database::paramsToConditions($params);
        $referencedCodeListValues = empty($codeListOptionsIds) ? [] :
            Mbase2Database::query("SELECT clo.*,cl.list_id list_id FROM mbase2.code_list_options clo, (SELECT cl.id, clv.id list_id FROM mbase2.code_list_options clv, mbase2.code_lists cl WHERE clv.key = cl.label_key AND $where) cl WHERE cl.id = clo.list_id", $params);

        $res = empty($referencedTableIds) ? [] : Mbase2Database::select('mbase2.referenced_tables',[':id' => $referencedTableIds]);
        $tableNameAssocId = [];
        foreach($res as &$r) {
            $listId = $r['key_id'] = $clId[$r['id']];
            $tableNameAssocId[$listId] = $r['id'];
        }

        $camelotReferencesOverrides = [];

        if (!is_null($camelotId)) {
            foreach ([
                'ct_trap_stations' => 'camelot_trap_station_id',
                'ct_sites' => 'camelot_site_id',
                'ct_cameras' => 'camelot_camera_id',
                'ct_surveys' => 'camelot_survey_id'] 
                as $tname => $key) {
                
                    $listId = $tableNameAssocId[$tname];
                    $camelotReferencesOverrides[$tname] = [
                        'sql' => "SELECT id as id, $listId as list_id, $key as key from mb2data.$tname WHERE camelot_id=:camelot_id",
                        'params' => [':camelot_id' => $camelotId]
                    ];
            }
        }

        $referencedTableValues = Mbase2Database::getReferencedValues($res, $camelotReferencesOverrides);

        function getReferencedValues(&$references, $refListId) {
            $values = [];
            foreach ($references as $i => $ref) {
                if ($ref['list_id'] === $refListId) {
                    $values[strtolower($ref['key'])] = $ref['id'];
                    unset($references[$i]);
                }
            }
            return $values;
        }

        foreach($attributes as &$a) {
            if ($a['data_type'] === 'code_list_reference' || $a['data_type'] === 'code_list_reference_array') {
                $a['values'] = getReferencedValues($referencedCodeListValues, $a['ref']);
            }
            else if ($a['data_type'] === 'table_reference' || $a['data_type'] === 'table_reference_array') {
                $a['values'] = getReferencedValues($referencedTableValues, $a['ref']);
            }
            else if ($a['data_type'] === 'location_reference') {
                $a['_locations_table'] = $clId[$a['ref']];
                if (empty($a['_locations_table'])) {
                    $a['_locations_table'] = $this->keyModuleId.'_locations';
                }
            }
        }

        unset($a);

        $this->data = [];

        $batchData = [];

        if (is_null($camelotId)) {
            list($filePath, $fileName, $fileHash) = Mbase2Files::getFileData(intval($dsid));
            $ext = strtolower(pathinfo($fileName, PATHINFO_EXTENSION));
            $batchData['dsid'] = $dsid;
            $batchData['sheetInx'] = $sheetInx;
            $batchData['fileName'] = $fileName;

            if ($ext === 'csv') {
                $this->readCSV($filePath.$fileHash, $this->data);
            }
            else {
                $this->readExcel($filePath.$fileHash, $this->data, $sheetInx);
            }
        }
        else {
            $batchData['camelotId'] = $camelotId;
            $this->data = $camelotData;
            $dataDefinitions = $this->camelotDataDefinitions($uid);
        }

        $batchData['dataDefinitions'] = $dataDefinitions;
        
        $indices=[];
        $values = $dataDefinitions[$keyModuleId]['values'];
        $types = $dataDefinitions[$keyModuleId]['types'];

        $batchId = null;

        if (isset($attributes['_batch_id'])) {
            list($res) = Mbase2Database::query("INSERT INTO mbase2.import_batches (user_id) VALUES (:user_id) RETURNING id",[':user_id'=>$uid]);

            $batchId = $res['id'];
            $values['_batch_id'] = $batchId;
            $types['_batch_id'] = 'fixed';
        }

        $fixedValues = [];
        $placeholderAttributes = [];

        foreach($attributes as $aname=>$a) {
            $type = $types[$aname];
            if (empty($type) && $aname!=='_uname') continue;
            $placeholder = ':'.$aname;
            $placeholderAttributes[$placeholder] = $a;

            if ($type === 'fixed') {
                $indices[$placeholder] = false;
                $fixedValues[$placeholder] = $values[$aname];
            }
            else {
                $indices[$placeholder]=$values[$aname];
            }
        }

        $this->placeholderAttributes = $placeholderAttributes;

        $errors = [];

        Mbase2Database::query("BEGIN");

        if (!is_null($camelotId)) {
            Mbase2Database::query("DELETE FROM mb2data.ct WHERE 
            _batch_id in (select id from mbase2.import_batches where (data->>'camelotId')::integer = :camelot_id)", [':camelot_id' => $camelotId]);
        }

        $crows = count($this->data);

        $insertedRows = 0;
        
        for ($rinx = 1; $rinx < $crows; $rinx++) {

            $row = $this->data[$rinx];
            $values = [];
            
            $this->currentRow = $row;

            $rowErrors = [];

            foreach($indices as $placeholder => $cinx) {

                $currentAttributeProperties = $placeholderAttributes[$placeholder];
                $dataType = $currentAttributeProperties['data_type'];
                $validationResult = null;

                if ($placeholder === ':_uname') {
                    $value = 1;
                    $validationResult = true;
                }
                else if ($cinx === false) {
                    $value = $fixedValues[$placeholder];
                }
                else {

                    if ($dataType === 'location_reference' || $dataType === 'location_geometry') {
                        $value = [
                            'lat' => $row[$cinx['lat']],
                            'lon' => $row[$cinx['lon']],
                            '_locations_table' => $currentAttributeProperties['_locations_table']
                        ];
                    }
                    else {
                        $value = trim($row[$cinx]);
                    }

                    if (!empty($value)) {
                        if ($dataType === 'code_list_reference' || $dataType === 'table_reference' || $dataType === 'table_reference_array' || $dataType === 'code_list_reference_array') {
                            
                            $refs = $currentAttributeProperties['values'];

                            if ($dataType === 'table_reference_array' || $dataType === 'code_list_reference_array') {
                                $rvalues = json_decode($value);
                                if (empty($rvalues)) $rvalues = json_decode('["'.$value.'"]');
                                
                                if (!empty($rvalues) && count($rvalues)>0) {
                                    $value = [];
                                    foreach($rvalues as $v) {
                                        $lvalue = strtolower($v);
                                        $value[] = isset($refs[$lvalue]) ? $refs[$lvalue] : null;        
                                    }
                                    $value = json_encode($value);
                                }
                                else {
                                    $value = null;
                                }
                            }
                            else {
                                $lvalue = strtolower($value);
                                $value = isset($refs[$lvalue]) ? $refs[$lvalue] : null;
                            }
                        }
                        else if ($dataType === 'date' || $dataType === 'time') {
                            if (self::isTimestampIsoValid($value)) {    //YYYY-MM-DD or YYYY-MM-DD HH:mm:ss
                                $validationResult = true;
                            }
                            else if (is_numeric($value)){   //excel values - don't know why is_int does not work, so I am using is_numeric
                                $value = self::excelDate2iso($value);
                                if ($value !== false) $validationResult = true;
                            }
                            else if ($dataType === 'time') {    //string time HH:mm:ss
                                $value = self::explodeTimeStamp($dataType, $value);
                            }

                            if ($validationResult === true) {
                                $value = self::explodeTimeStamp($dataType, $value);
                            }
                        }
                        else if ($dataType === 'image_array') {
                            $fileNames = explode(',', $value);
                            $value = [];
                            foreach ($fileNames as $fileName) {
                                if (isset($media[$fileName])) {
                                    $value[] = $media[$fileName];
                                }
                            }

                            $value = empty($value) ? null : json_encode($value);
                        }
                    }
                    else {
                        $value = null;
                    }
                }

                $required = intval($this->placeholderAttributes[$placeholder]['required']) === 1;

                if (is_null($validationResult)) {
                    $validationResult = $this->validateAttributeValue($placeholder, $dataType, $value, $rinx, $cinx, $required);
                }

                if ($validationResult !== true) {
                    $rowErrors[] = $validationResult;
                }

                if ($required && is_null($value)) continue;

                $values[$placeholder] = $value;
            }

            if (empty($rowErrors)) {
                try {
                    Mbase2Database::insert("mb2data.$keyModuleId", $values);
                    $insertedRows++;
                }
                catch (Exception $e) {
                    Mbase2Database::query("ROLLBACK");
                    $errors[] = $this->errorMessage(null, null, null, $e->getMessage(), null);
                }
            }
            else {
                $errors[] = $rowErrors;
            }   
        }

        Mbase2Database::query("COMMIT");

        if (count($errors) > 0 && !is_null($batchId)) {
            Mbase2Database::query("BEGIN");
            foreach($errors as $error) {
                try {
                    Mbase2Database::query("INSERT INTO mbase2.import_errors (batch_id, data) VALUES (:batch_id, :data)", [':batch_id' => $batchId, ':data' => json_encode($error)]);
                }
                catch (Exception $e) {
                    Mbase2Database::query("ROLLBACK");
                    throw new Exception($e->getMessage());  
                }
            }
            Mbase2Database::query("COMMIT");
        }

        if (!is_null($batchId)) {
            $batchData['crows'] = $crows - 1;
            $batchData['insertedRows'] = $insertedRows;
            Mbase2Database::query("UPDATE mbase2.import_batches SET data = :data WHERE id=:id",[':id'=>$batchId, ':data' => json_encode($batchData)]);
        }

        $this->errors = $errors;
    }

    public static function explodeTimeStamp($dataType, $value) {
        $value = preg_replace('!\s+!', ' ', $value); //replace multiple spaces with a single space

        $value = explode(' ', $value);
        if (count($value)===1) {
            $value = $value[0];
        }
        else {
            if ($dataType === 'date') {
                $value = $value[0];
            }
            else {
                $value = $value[1];
            }
        }

        return $value;
    }

    public static function isTimestampIsoValid($timestamp){
        $format = "Y-m-d";
        if (strpos($timestamp, ':') !== FALSE) $format = "Y-m-d H:i:s";
        $dt = DateTime::createFromFormat($format, $timestamp);
        return $dt !== false && !array_sum($dt::getLastErrors());
      }

    public static function excelDate2iso($date){
        if (empty($date)) return null;
        $UNIX_DATE = ($date - 25569) * 86400;
        return gmdate("Y-m-d H:i:s", $UNIX_DATE);
      }

    function camelotDataDefinitions($uid) {
        $dataDefinitions = [
            $this->keyModuleId => ['types' => [], 'values' => []]
        ];

        if (count($this->data) < 1) return $dataDefinitions;

        $header = $this->data[0];

        $types = [
            "_uname" => "fixed",
            "_species_name"=>"column",
            "_licence_name"=>"fixed",
            "event_date"=>"column",
            "photos"=>"column",
            "sighting_quantity"=>"column",
            "trap_station_name"=>"column",
            "event_time"=>"column",
            "sex"=>"column",
            "individual_name"=>"column",
            "life_stage"=>"column",
            "position"=>"column",
            "hair_trap"=>"column",
            "notes"=>"column",
            "survey_name"=>"column",
            "camera_name"=>"column"
        ];

        $dataDefinitions[$this->keyModuleId]['types'] = $types;

        $typeValues = [
            "_species_name" => "Species Common Name",
            "event_date"=>"Date/Time",
            "photos"=>"Media Filename",
            "sighting_quantity"=>"Sighting Quantity",
            "trap_station_name"=>"Trap Station ID",
            "event_time"=>"Date/Time",
            "sex"=>"Sex",
            "individual_name"=>"Individual name",
            "life_stage"=>"Life stage",
            "position"=>"Position",
            "hair_trap"=>"Hair trap",
            "notes"=>"Remarks",
            "survey_name"=>"Survey ID",
            "camera_name"=>"Camera ID"
        ];

        foreach ($types as $cname => $type) {
            if ($type === 'fixed') continue;
            $typeValues[$cname] = array_search($typeValues[$cname], $header);

        }

        $typeValues["_licence_name"] = 10; //Mbase licence
        $typeValues["_uname"] = $uid; //user id
        
        $dataDefinitions[$this->keyModuleId]['values'] = $typeValues;
                
        return $dataDefinitions;
    }

    function validateAttributeValue($placeholder, $dataType, $value, $rinx, $cinx, $required) {

        $validationResult = true;

        if (empty($value) && !$required) {
            return true;
        }

        if (is_null($value) && $required) {
            $validationResult = "required value is null";
        }
        else if ($dataType === 'code_list_reference' || $dataType === 'table_reference' || $dataType === 'table_reference_array' || $dataType === 'code_list_reference_array') {
            if (empty($value) && $value !== 0) {
                $validationResult = 'missing reference';
            }
        }
        else if ($dataType === 'integer') {
            $tval = intval($value);
            if ($tval != $value) {
                $validationResult = 'not an integer';
            }
        }
        else if ($dataType === 'real') {
            if (!is_numeric($value)) {
                $validationResult = 'not a numeric';
            }
        }
        else if ($dataType === 'text') {
            ;
        }
        else if ($dataType === 'jsonb') {
            if (is_null(json_decode($value))) {
                $validationResult = 'not a valid json';
            }
        }
        else if ($dataType === 'image') {
        }
        else if ($dataType === 'image_array') {
            if (is_null($value)) {
                $validationResult = 'missing image';
            }
        }
        else if ($dataType === 'location_reference') {
            //TODO
        }
        else if ($dataType === 'time') {
            if (!preg_match("/^(?:2[0-3]|[01][0-9]):[0-5][0-9]:[0-5][0-9]$/", $value)) {
                $validationResult = 'the time is not int the HH:MM:SS format';
            }
        }
        else if ($dataType === 'date') {
            $dt = DateTime::createFromFormat("Y-m-d", $value);
            if ($dt === false) {
                $validationResult = 'the date is not in the YYYY-MM-DD format';
            }
            else {
                //array_sum trick is a terse way of ensuring that PHP did not do "month shifting" (e.g. consider that January 32 is February 1)
                //cf. https://stackoverflow.com/questions/13194322/php-regex-to-check-date-is-in-yyyy-mm-dd-format
                $monthShifting = array_sum($dt::getLastErrors());

                if ($monthShifting) $validationResult = 'wrong date';
            }
        }

        return $validationResult === true ? true : $this->errorMessage($placeholder, $rinx, $cinx, $validationResult, $required);
    }

    /**
     * Helper function: generates consistent error message
     * @param {integer} $rinx row index
     * @param {integer} $cinx column index
     * @param {string} $key_name_id variable name
     */

    private function errorMessage($placeholder, $rinx, $cinx, $error, $required) {
        $value = $this->currentRow[$cinx];
        $variableKey = ltrim($placeholder,':');
        $err = [
            'row'=> $rinx,
            'column'=> $cinx,
            'value' => $value,
            'variable' => $variableKey,
            'error' => $error,
            'required' => $required
        ];
        return json_encode($err);
    }

    private function readExcel($filePath, &$data, $sheetInx) {
        $reader = new Reader();
        $reader->open($filePath);
        $reader->changeSheet($sheetInx);

        foreach ($reader as $row) {
            $data[] = $row;
        }

        $reader->close();
    }

    function readCSV($filePath, &$data) {
        $i = 0;
        if (($handle = fopen($filePath, "r")) !== FALSE) {
            while (($row = fgetcsv($handle, 2048, ",")) !== FALSE) {
                $data[$i] = $row;
                $i++;
            }
            fclose($handle);
        }
    }

    
}

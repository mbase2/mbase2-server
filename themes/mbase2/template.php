<?php
function mbase2_js_alter(&$javascript) {
  
  if (arg(0) !== 'mbase2') return;
  
  $cycle = $javascript['sites/all/themes/corporateclean/js/jquery.cycle.all.js'];

  foreach($javascript as $path => $js) {

    if ((strstr($path, 'themes/gicc') === FALSE &&
        strstr($path, 'misc/jquery.js') === FALSE &&
        strstr($path, '8081') === FALSE &&
        strstr($path, 'mbase2') === FALSE &&
        $js['type'] !== 'inline') || strstr($path, 'gicc/lib/patternfly') !== FALSE
    ) {
        unset($javascript[$path]);
    }
  }

  $javascript['sites/all/themes/corporateclean/js/jquery.cycle.all.js'] = $cycle;
}

function mbase2_css_alter(&$css) {
  
  if (arg(0) !== 'mbase2') return;

  foreach($css as $path => $c) {
    if ((strstr($path, 'themes/gicc') === FALSE && 
        strstr($path, 'themes/corporateclean/color') === FALSE &&
        strstr($path, '8081') === FALSE &&
        strstr($path, 'mbase2') === FALSE) || strstr($path, 'gicc/lib/patternfly') !== FALSE
    ) {
        unset($css[$path]);
    }
  }
}

/*
 *  Remove labels and add HTML5 placeholder attribute to login form
 */
function mbase2_form_alter(&$form, &$form_state, $form_id) {
  if ( TRUE === in_array( $form_id, array( 'user_login', 'user_login_block') ) ) {
    if ($form['#action'] === '/user/login?destination=mbase2/camelot') {
      drupal_add_js("jQuery(function(){
        jQuery('.navbar-header').hide();
        jQuery('.element-invisible').hide();
        jQuery('ul.tabs.primary').hide();
        jQuery('div.breadcrumb').hide();
        jQuery('div#main').css('padding','10px').css('padding-top','64px');
      });",array('type' => 'inline', 'scope' => 'footer'));
    }
  }
}

function mbase2_menu_alter(&$callback)
{
   unset($callback['user/password']);
   unset($callback['user/register']);
}

/*
function mbase2_preprocess_page(&$vars) {
  //theme_hook_suggestions
}
*/

(function($){
    $(document).ready(function () {
        $('.navbar-header').removeClass('open');
        $('.species-selector').click(function(){
          var text=$(this).text();
          $('#species-selector-text').text(text);
          $(this).parent().parent().find('a').removeClass('bold');
          $(this).addClass('bold');
        });
    });
}(jQuery));
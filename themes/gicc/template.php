<?php
function gicc_js_alter(&$javascript) {
  $theme_path=drupal_get_path('theme', 'gicc');
	$jquery_path = $theme_path . '/lib/jquery-1.11.3.min.js';
 
  //We duplicate the important information from the Drupal one
  $javascript[$jquery_path] = $javascript['misc/jquery.js'];
  //..and we update the information that we care about
  $javascript[$jquery_path]['version'] = '1.11.3';
  $javascript[$jquery_path]['data'] = $jquery_path;
	
	$jquery_migrate = $theme_path . '/lib/jquery-migrate-1.4.1.js';
	
	$javascript[$jquery_migrate] = $javascript['misc/jquery.js'];
	$javascript[$jquery_migrate]['version'] = '1.4.1';
	$javascript[$jquery_migrate]['data'] = $jquery_migrate;
	$javascript[$jquery_migrate]['weight']=$javascript[$jquery_migrate]['weight']+0.001;
 
  //Then we remove the Drupal core version
  unset($javascript['misc/jquery.js']);
}

function gicc_menu_tree__bears_menu($variables) {
  return '<ul class="dropdown-menu">' . $variables['tree'] . '</ul>';
}

function gicc_menu_link__bears_menu($variables){
	$element = $variables['element'];
  $sub_menu = '';
	$liClass='';

  if ($element['#below']) {
    $sub_menu = drupal_render($element['#below']);
		$liClass=' class="dropdown-submenu"';
  }
  $output = l($element['#title'], $element['#href'], $element['#localized_options']);
  return "<li$liClass>" . $output . $sub_menu . "</li>\n";
}

function gicc_preprocess_html(&$vars) {
  $viewport = array(
   '#tag' => 'meta',
   '#attributes' => array(
     'name' => 'viewport',
     'content' => 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no',
   ),
  );
  drupal_add_html_head($viewport, 'viewport');
}
<?php
	$base=file_create_url('');
  
  $availableLanguages=language_list();
  $defaultLanguage=language_default('language');
  global $language;
  $currentLanguage=$language->language;
  $languageMenu=[];
  global $base_url;
  foreach($availableLanguages as $lcode=>$lang){
    if ($lcode==$defaultLanguage){
      $prefix='';
    }
    else{
      $prefix=$lcode;
    }
    
    if ($lcode==$currentLanguage){
      $active=' active';
    }
    else{
      $active='';
    }
    
    $languageMenu[]='<a class="language-link'.$active.'" href="'.$base_url.'/'.$prefix.'">'.strtoupper($lcode).'</a>';
  }
  
  $species= _get_code_list(null,'species');
  $params=[':lang'=>$currentLanguage];
  
  if (!isset($_SESSION['species']) || empty($_SESSION['species'])) {
    $cspecies=t('all');
    $specSelector='';
  }
  else{
    $cspecies=$_SESSION['species'];
    $specSelector=' and species=:spec';
    $params[':spec']=$cspecies;
  }
  
  $speciesMenu=[];
  foreach($species as $i=>$s){
    $speciesMenu[]='<a href="?species='.$i.'">'.$s.'</a>';
  }
  
  $speciesAll='<li><a href="?species=">'.t('all').'</a></li>';
  
  //logo
  $logo=db_query("select logo from cdb.overrides where language=:lang$specSelector",$params)->fetchField();
  if (empty($logo)) {
    $logo=db_query("select logo from cdb.overrides where (language is null or language='') and (species is null)")->fetchField();  
  }
  
  $logo=$base.'sites/default/files/images/'.$logo;
  
?>
<!-- #header -->
<div id="header" class="pft">
  <nav class="navbar navbar-fixed-top navbar-pf" role="navigation">
		<div class="navbar-header" style="width:100%">
			
      <?php if (!user_is_anonymous()):?>
        <ul class="nav navbar-nav" id="app-menu">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-bars fa-lg" aria-hidden="true"></i>
          </a>
          <?php
            if (module_exists('i18n_menu')) {
              $main_menu_tree = i18n_menu_translated_tree(variable_get('menu_main_links_source', 'bears-menu'));
            }
            else {
              $main_menu_tree = menu_tree(variable_get('menu_main_links_source', 'bears-menu')); 
            }
            print drupal_render($main_menu_tree);
          ?>
        </li>
        </ul>
      <?php endif; ?>
      
			<a href="<?php echo $base;?>" class="navbar-brand">
			  <img class="navbar-brand-icon" src="<?php echo $logo;?>" alt=""/>
			</a>

			<h4 class="navbar-text navbar-header pull-right">
				<?php
					if (!user_is_anonymous()){
						global $user;
						echo "<a href='{$base}user/logout'>".t('Log out')." ({$user->name})</a>";
					}else{
						echo "<a href='{$base}user'>".t('Login')."</a>";
					}
				?>
			</h4>
      
      <h4 class="navbar-text navbar-header pull-right">
        <?php
          echo implode('&nbsp;|&nbsp;',$languageMenu);
        ?>
			</h4>
      
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo t('species').' ('.($cspecies==t('all')?t('all'):$species[$cspecies]).')';?> <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <?php
              echo '<li>'.implode('</li><li>',$speciesMenu).'</li><li role="separator" class="divider"></li>'.$speciesAll;
            ?>
          </ul>
        </li>
      </ul>
      
		</div>
	</nav> <!--/.navbar-->
</div><!-- EOF: #header -->

<!-- #content -->
<div id="content">
	<!-- #content-inside -->
    <div id="content-inside" class="container_12 clearfix">
    
        <?php if ($page['sidebar_first']) :?>
        <!-- #sidebar-first -->
        <div id="sidebar-first" class="grid_4">
        	<?php print render($page['sidebar_first']); ?>
        </div><!-- EOF: #sidebar-first -->
        <?php endif; ?>
        
        <?php if ($page['sidebar_first'] && $page['sidebar_second']) { ?>
        <div class="grid_4">
        <?php } elseif ($page['sidebar_first'] || $page['sidebar_second']) { ?>
        <div id="main" class="grid_8">
		<?php } else { ?>
        <div id="main" class="grid_12">    
        <?php } ?>
            
            <?php if (theme_get_setting('breadcrumb_display','corporateclean')): print $breadcrumb; endif; ?>
            
            <?php if ($page['highlighted']): ?><div id="highlighted"><?php print render($page['highlighted']); ?></div><?php endif; ?>
       
            <?php if ($messages): ?>
            <div id="console" class="clearfix">
            <?php print $messages; ?>
            </div>
            <?php endif; ?>
     
            <?php if ($page['help']): ?>
            <div id="help">
            <?php print render($page['help']); ?>
            </div>
            <?php endif; ?>
            
            <?php if ($action_links): ?>
            <ul class="action-links">
            <?php print render($action_links); ?>
            </ul>
            <?php endif; ?>
            
			<?php print render($title_prefix); ?>
            <?php if ($title): ?>
            <h1><?php print $title ?></h1>
            <?php endif; ?>
            <?php print render($title_suffix); ?>
            
            <?php if ($tabs): ?><?php print render($tabs); ?><?php endif; ?>
            
            <?php print render($page['content']); ?>
            
            <?php print $feed_icons; ?>
            
        </div><!-- EOF: #main -->
        
        <?php if ($page['sidebar_second']) :?>
        <!-- #sidebar-second -->
        <div id="sidebar-second" class="grid_4">
        	<?php print render($page['sidebar_second']); ?>
        </div><!-- EOF: #sidebar-second -->
        <?php endif; ?>  

    </div><!-- EOF: #content-inside -->

</div><!-- EOF: #content -->